#version 330 core

in vec4 position;
in vec4 color;
in vec4 normal;

struct Light{
	int type; // 0 = directional, 1 = point light
	vec3 position;
	vec3 direction;
};

uniform int numLights;
uniform Light[16] lights;
uniform mat4 projection;
uniform mat4 modelview;
uniform sampler2D sampler;

out vec4 outColor;

void main(){
#ifdef ENABLE_LIGHT
	outColor = vec4(0, 0, 0, 1);
	for(int i=0;i<numLights;i++){
		Light light=lights[i];
		vec4 lightPosition = projection*modelview*vec4(light.position.x, light.position.y, light.position.z, 0);
		vec4 diff = lightPosition - position;
		float lightDistance = length(diff);
		diff = normalize(diff);
		float attenuation = max(dot(normal, diff), 0.0);
		outColor += color * attenuation;
		outColor.w = color.w;
	}
#else
	outColor = color;
#endif
}