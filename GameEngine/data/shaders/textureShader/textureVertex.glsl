#version 330 core

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec2 inTex;

out vec4 position;
out vec4 normal;
out vec2 tex;

uniform mat4 projection;
uniform mat4 modelview;

void main(){
  // Apply transformation matrices to vertex
  gl_Position = projection * modelview * vec4(inPosition.x, inPosition.y, inPosition.z, 1.0);
  
  // Pass output to fragment shader
  position = gl_Position;
  normal = normalize(projection * modelview * vec4(inNormal.x, inNormal.y, inNormal.z, 0.0));
  tex = inTex.xy;
}