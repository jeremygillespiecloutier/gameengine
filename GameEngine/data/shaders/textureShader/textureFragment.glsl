#version 330 core

in vec4 position;
in vec4 normal;
in vec2 tex;

struct Light{
	int type; // 0 = directional, 1 = point light
	vec3 position;
	vec3 direction;
};

uniform int numLights;
uniform Light[16] lights;
uniform mat4 projection;
uniform mat4 modelview;
uniform sampler2D sampler;

out vec4 outColor;


void main(){
#ifdef ENABLE_LIGHT
	vec4 color = texture(sampler, tex);
	outColor = vec4(0, 0, 0, 1);
	float linearCoeff = 0.05;
	float expCoeff = 0.005;
	for(int i=0;i<numLights;i++){
		Light light = lights[i];

		// Point light calculations
		vec4 lightPosition = projection*modelview*vec4(light.position.x, light.position.y, light.position.z, 0);
		vec4 diff = lightPosition - position;
		float lightDistance = length(diff);
		diff = normalize(diff);
		float attenuation = max(dot(normal, diff), 0.0) / (1.0 + linearCoeff*lightDistance + expCoeff*lightDistance*lightDistance);
		outColor += color * attenuation * light.type;

		// Directional light calculations
		vec4 direction = vec4(light.direction.x, light.direction.y, light.direction.z, 1);
		outColor += color * max(dot(normal, direction), 0.0) * (1 - light.type);
	}
#else
	outColor = texture(sampler, tex);
#endif
}