#include "GameMesh.h"
#include <glew.h>
#include <freeglut.h>
#include "GameRenderer.h"

namespace engine {

	GameMesh::GameMesh(std::vector<Vector3> vertices, std::vector<Vector3> vertexNormals, std::vector<MeshFace> faces,
		std::vector<Vector3> textureVertices, std::map<std::string, GameMaterial> materials)
		:vertices(vertices), vertexNormals(vertexNormals), faces(faces), textureVertices(textureVertices), materials(materials) { }

	void GameMesh::display() {
		GameRenderer::beginRender(RenderType::Triangles);
		for (unsigned i = 0; i < faces.size(); i++) {
			MeshFace face = faces[i];
			if (face.hasMaterial) {
				GameMaterial material = materials[face.materialName];
				const GameTexture &texture = material.getTexture();
				if (texture.isInitialized()) {
					GameRenderer::useTexture(texture);
				}
			}
			for (unsigned j = 0; j < 3; j++) {
				Vector3 vertex = vertices[face.vertices[j]];
				if (face.hasNormals) {
					Vector3 vertexNormal = vertexNormals[face.vertexNormals[j]];
					GameRenderer::setNormal(vertexNormal);
				}
				if (face.hasTexture) {
					Vector3 textureCoord = textureVertices[face.textureVertices[j]];
					GameRenderer::setTextureCoord(textureCoord);
				}
				GameRenderer::pushVertex(vertex);
			}
		}
		GameRenderer::endRender();
	}

	void GameMesh::update(unsigned deltaTime) {

	}

}