/*
Author: Jeremy Gillespie-Cloutier
Implementation of the GameShader class
*/
#include "GameShader.h"
#include <fstream>
#include <sstream>
#include <ios>
#include <exception>
#include <vector>
#include "GameRenderer.h"
#include <regex>

namespace engine {

	// Creates a shader using the given glsl files
	// vertexPath: The path to the file that contains the glsl vertex shader code
	// fragmentPath: The path to the file that contains the glsl fragment shader code
	// [throw]: Exception when the path is invalid
	GameShader::GameShader(std::string vertexPath, std::string fragmentPath, std::vector<std::string> defineList) : defineList(defineList) {

		// Load shaders
		vertexShader = loadShader(vertexPath, true);
		fragmentShader = loadShader(fragmentPath, false);

		// Create the shader program
		program = glCreateProgram();
		glAttachShader(program, vertexShader);
		glAttachShader(program, fragmentShader);
		glLinkProgram(program);

		// Check for errors
		GLint success;
		glGetProgramiv(program, GL_LINK_STATUS, &success);
		if (!success) {
			// If linking was not successful, get the error message
			GLint messageLength = 0;
			glGetProgramiv(program, GL_INFO_LOG_LENGTH, &messageLength);

			std::vector<GLchar> messageData(messageLength);
			glGetProgramInfoLog(program, messageLength, &messageLength, &messageData[0]);

			std::string message = "Error linking the shader. The error message is: ";
			for (GLchar c : messageData) {
				message += c;
			}
			throw std::exception(message.c_str());
		}
	}

	// Loads the given shader file into a shader and returns its handle
	// path: The path to the shader file
	// isVertex: If true, loads a vertex shader, otherwise a fragment shader
	// [throw]: Exception when the path is invalid
	// [throw]: When the shader code cannot be compiled
	// [return]: The shader handle
	GLuint GameShader::loadShader(std::string path, bool isVertex) {
		GLuint shader = glCreateShader(isVertex ? GL_VERTEX_SHADER : GL_FRAGMENT_SHADER);
		std::ifstream file(path);

		if (file.fail()) {
			throw std::exception("Error loading shader source.");
		}

		// Put shader source code in a string
		std::stringstream content;
		content << file.rdbuf();
		std::string shaderStr = content.str(); // Contains the shader source code

		// Convert the define list into some glsl code
		std::string defineStr = "";
		for (std::string &defineVal : defineList) {
			defineStr = defineStr + "#define " + defineVal + "\n";
		}

		std::regex versionPattern("^\\s*#version.+$"); // This matches lines like "#version 330 core"
		std::cmatch match;

		// If there is a version specified in the source code, our own defines must be added after it
		if (std::regex_search(shaderStr.c_str(), match, versionPattern)) {
			std::string replacementStr = std::string(match[0]) + "\n" + defineStr;
			shaderStr = std::regex_replace(shaderStr.c_str(), versionPattern, replacementStr.c_str());
		}
		else { // Otherwise they can be added at the start of the source code
			shaderStr = defineStr + shaderStr;
		}

		const char *source = shaderStr.c_str();

		glShaderSource(shader, 1, &source, NULL);
		glCompileShader(shader);

		GLint success;
		glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
		if (!success) {
			// If compilation was not successful, get the error message
			GLint messageLength = 0;
			glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &messageLength);

			std::vector<GLchar> messageData(messageLength);
			glGetShaderInfoLog(shader, messageLength, &messageLength, &messageData[0]);

			std::string message = "Error compiling the shader. The error message is: ";
			for (GLchar c : messageData) {
				message += c;
			}
			throw std::exception(message.c_str());
		}

		file.close();

		return shader;
	}

	void GameShader::doApply() {
		apply();
	}

	void GameShader::setUniform(std::string uniform, float value) {
		GLint location = glGetUniformLocation(program, uniform.c_str());
		glUniform1f(location, value);
	}

	void GameShader::setUniform(std::string uniform, std::vector<float> values) {
		GLint location = glGetUniformLocation(program, uniform.c_str());
		glUniform1fv(location, values.size(), &values[0]);
	}

	void GameShader::setUniform(std::string uniform, int value) {
		GLint location = glGetUniformLocation(program, uniform.c_str());
		glUniform1i(location, value);
	}

	void GameShader::setUniform(std::string uniform, std::vector<int> values) {
		GLint location = glGetUniformLocation(program, uniform.c_str());
		glUniform1iv(location, values.size(), &values[0]);
	}

	void GameShader::setUniform(std::string uniform, Vector3 value) {
		GLint location = glGetUniformLocation(program, uniform.c_str());
		glUniform3f(location, (GLfloat)value.getX(), (GLfloat)value.getY(), (GLfloat)value.getZ());
	}

	void GameShader::setUniform(std::string uniform, std::vector<Vector3> values) {
		GLint location = glGetUniformLocation(program, uniform.c_str());
		std::vector<float> components;
		for (Vector3 &value : values) {
			components.push_back((GLfloat)value.getX());
			components.push_back((GLfloat)value.getY());
			components.push_back((GLfloat)value.getZ());
		}
		glUniform3fv(location, values.size(), &components[0]);
	}

	void GameShader::setUniform(std::string uniform, FixedMatrix<4, 4> value) {
		GLfloat matrix[16];
		for (unsigned i = 0; i < 4; i++) {
			for (unsigned j = 0; j < 4; j++) {
				matrix[i * 4 + j] = (GLfloat)value.get(i, j);
			}
		}
		GLint location = glGetUniformLocation(program, "projection");
		glUniformMatrix4fv(location, 1, false, matrix);
	}

	void GameShader::setLights(std::vector<const GameLight*> lights) {
		this->lights = lights;
	}

	ColorShader::ColorShader(std::string vertexPath, std::string fragmentPath, std::vector<std::string> defineList)
		: GameShader(vertexPath, fragmentPath, defineList) { }

	void ColorShader::apply() {

		const std::vector<double> &vertexBuffer = GameRenderer::getVertexBuffer();
		GLuint vertexBufferID = GameRenderer::getVertexBufferID();

		const std::vector<double> &colorBuffer = GameRenderer::getColorBuffer();
		GLuint colorBufferID = GameRenderer::getColorBufferID();

		const std::vector<double> &normalBuffer = GameRenderer::getNormalBuffer();
		GLuint normalBufferID = GameRenderer::getNormalBufferID();

		glUseProgram(program);
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);

		// Pass vertices to shader
		glBindBuffer(GL_ARRAY_BUFFER, vertexBufferID);
		glBufferData(GL_ARRAY_BUFFER, vertexBuffer.size() * sizeof(double), &vertexBuffer[0], GL_DYNAMIC_DRAW);
		glVertexAttribPointer(0, 3, GL_DOUBLE, GL_FALSE, 0, 0);

		// Pass colors to shader
		glBindBuffer(GL_ARRAY_BUFFER, colorBufferID);
		glBufferData(GL_ARRAY_BUFFER, colorBuffer.size() * sizeof(double), &colorBuffer[0], GL_DYNAMIC_DRAW);
		glVertexAttribPointer(1, 4, GL_DOUBLE, GL_FALSE, 0, 0);

		// Pass vertex normals to shader
		glBindBuffer(GL_ARRAY_BUFFER, normalBufferID);
		glBufferData(GL_ARRAY_BUFFER, normalBuffer.size() * sizeof(double), &normalBuffer[0], GL_DYNAMIC_DRAW);
		glVertexAttribPointer(2, 3, GL_DOUBLE, GL_TRUE, 0, 0);

		// Pass projection matrix
		GLfloat matrix[16];
		glGetFloatv(GL_PROJECTION_MATRIX, matrix);
		GLint location = glGetUniformLocation(program, "projection");
		glUniformMatrix4fv(location, 1, false, matrix);

		// Pass modelview matrix
		glGetFloatv(GL_MODELVIEW_MATRIX, matrix);
		location = glGetUniformLocation(program, "modelview");
		glUniformMatrix4fv(location, 1, false, matrix);

		location = glGetUniformLocation(program, "numLights");
		GLint numLights = 1;
		glUniform1i(location, numLights);

		location = glGetUniformLocation(program, "lightPositions");
		GLfloat lightPositions[] = { 0, 5, 5 };
		glUniform3fv(location, 1, &lightPositions[0]);

		// Render geometry
		glDrawArrays(GameRenderer::getRenderType(), 0, vertexBuffer.size() / 3); // Starting from vertex 0; 3 vertices total -> 1 triangle

		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);
	}

	TextureShader::TextureShader(std::string vertexPath, std::string fragmentPath, std::vector<std::string> defineList)
		: GameShader(vertexPath, fragmentPath, defineList) { }

	// Renders the vertices currently in the vertex buffer.
	void TextureShader::apply() {

		const std::vector<double> &vertexBuffer = GameRenderer::getVertexBuffer();
		GLuint vertexBufferID = GameRenderer::getVertexBufferID();

		const std::vector<double> &normalBuffer = GameRenderer::getNormalBuffer();
		GLuint normalBufferID = GameRenderer::getNormalBufferID();

		const std::vector<double> &textureBuffer = GameRenderer::getTextureBuffer();
		GLuint textureBufferID = GameRenderer::getTextureBufferID();

		glUseProgram(program);
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);

		// Pass vertices to shader
		glBindBuffer(GL_ARRAY_BUFFER, vertexBufferID);
		glBufferData(GL_ARRAY_BUFFER, vertexBuffer.size() * sizeof(double), &vertexBuffer[0], GL_DYNAMIC_DRAW);
		glVertexAttribPointer(0, 3, GL_DOUBLE, GL_FALSE, 0, 0);

		// Pass colors to shader
		glBindBuffer(GL_ARRAY_BUFFER, normalBufferID);
		glBufferData(GL_ARRAY_BUFFER, normalBuffer.size() * sizeof(double), &normalBuffer[0], GL_DYNAMIC_DRAW);
		glVertexAttribPointer(1, 3, GL_DOUBLE, GL_FALSE, 0, 0);

		// Pass texture coordinates to shader
		glBindBuffer(GL_ARRAY_BUFFER, textureBufferID);
		glBufferData(GL_ARRAY_BUFFER, textureBuffer.size() * sizeof(double), &textureBuffer[0], GL_DYNAMIC_DRAW);
		glVertexAttribPointer(2, 2, GL_DOUBLE, GL_FALSE, 0, 0);

		// Pass projection matrix
		GLfloat matrix[16];
		glGetFloatv(GL_PROJECTION_MATRIX, matrix);
		GLint location = glGetUniformLocation(program, "projection");
		glUniformMatrix4fv(location, 1, false, matrix);

		// Pass modelview matrix
		glGetFloatv(GL_MODELVIEW_MATRIX, matrix);
		location = glGetUniformLocation(program, "modelview");
		glUniformMatrix4fv(location, 1, false, matrix);

		// Pass the sampler
		location = glGetUniformLocation(program, "sampler");
		glUniform1i(location, 0); // TODO: use real texture number instead of 0

		location = glGetUniformLocation(program, "numLights");
		GLint numLights = lights.size() > 16? 16:lights.size();
		glUniform1i(location, numLights);

		for (unsigned i = 0; i < lights.size() && i < 16; i++){
			const GameLight *light = lights[i];
			std::string prefix = "lights[" + std::to_string(i) + "].";

			bool directional = light->getType() == LightType::Directional;

			if (directional) {
				Vector3 direction = light->getDirection();
				location = glGetUniformLocation(program, (prefix + "direction").c_str());
				GLfloat lightDirection[] = { direction.getX(), direction.getY(), direction.getZ() };
				glUniform3fv(location, 1, &lightDirection[0]);
			}
			else {
				Vector3 position = light->getPosition();
				position = glGetUniformLocation(program, (prefix + "position").c_str());
				GLfloat lightPosition[] = { position.getX(), position.getY(), position.getZ() };
				glUniform3fv(location, 1, &lightPosition[0]);
			}

			location = glGetUniformLocation(program, (prefix + "type").c_str());
			GLint lightType = directional ? 0 : 1;
			glUniform1i(location, numLights);
		}

		// Render geometry
		glDrawArrays(GameRenderer::getRenderType(), 0, vertexBuffer.size() / 3); // Starting from vertex 0; 3 vertices total -> 1 triangle

		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);
	}

}