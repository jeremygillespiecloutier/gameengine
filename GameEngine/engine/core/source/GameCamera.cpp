/*
Author: Jeremy Gillespie-Cloutier
Implementation of the game camera class
*/
#include "GameCamera.h"
#include <freeglut.h>

namespace engine {

	GameCamera::GameCamera(ViewArea area, double leftDist, double rightDist, double bottomDist, double topDist, double nearDist, double farDist)
		: area(area), leftDist(leftDist), rightDist(rightDist), bottomDist(bottomDist), topDist(topDist), nearDist(nearDist), farDist(farDist) {}

	PerspectiveCamera::PerspectiveCamera() : GameCamera(ViewArea{ 0,0,0,0 }, 0, 0, 0, 0, 0, 0) {}

	PerspectiveCamera::PerspectiveCamera(ViewArea area, double leftDist, double rightDist, double bottomDist, double topDist, double nearDist, double farDist)
		: GameCamera(area, leftDist, rightDist, bottomDist, topDist, nearDist, farDist) {}

	void PerspectiveCamera::apply() const {
		glViewport((int)area.getX(), (int)area.getY(), (int)area.getWidth(), (int)area.getHeight());
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glFrustum(leftDist, rightDist, bottomDist, topDist, nearDist, farDist);
	}

	OrtographicCamera::OrtographicCamera(ViewArea area, double width, double height, double nearDist, double farDist)
		: GameCamera(area, 0, width, 0, height, nearDist, farDist) {
		this->leftDist = 0;
		this->rightDist = width;
		this->bottomDist = 0;
		this->topDist = height;
		this->nearDist = nearDist;
		this->farDist = farDist;
	}

	void OrtographicCamera::apply() const {
		glViewport((int)area.getX(), (int)area.getY(), (int)area.getWidth(), (int)area.getHeight());
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(leftDist, rightDist, bottomDist, topDist, nearDist, farDist);
	};

}