#include "GameLight.h"
#include <freeglut.h>

namespace engine {

	GameLight::GameLight(GameColor color, LightType type) : color(color), type(type) {}

	DirectionalLight::DirectionalLight(Vector3 direction, GameColor color) : GameLight(color, LightType::Point) {
		this->direction = direction;
	};

	PointLight::PointLight(Vector3 position, GameColor color) : GameLight(color, LightType::Point) {
		this->position = position;
	}
}