#include "GameMaterial.h"

namespace engine {

	void GameMaterial::setAmbientColor(GameColor ambientColor) {
		this->ambientColor = ambientColor;
	}

	void GameMaterial::setDiffuseColor(GameColor diffuseColor) {
		this->diffuseColor = diffuseColor;
	}

	void GameMaterial::setEmissiveColor(GameColor emissiveColor) {
		this->emissiveColor = emissiveColor;
	}

	void GameMaterial::setSpecularColor(GameColor specularColor) {
		this->specularColor = specularColor;
	}

	void GameMaterial::setAlpha(double alpha) {
		this->alpha = alpha;
	}

	void GameMaterial::setFlatLighting(bool flatLighting) {
		this->flatLighting = flatLighting;
	}

	void GameMaterial::setShine(double shine) {
		this->shine = shine;
	}

	void GameMaterial::setTexture(std::string file) {
		texture = GameTexture{ file };
	}

}