#include "GameTexture.h"

namespace engine {

	GameTexture::GameTexture(std::string file) {
		// Load the image from the disk and bind it to openGL
		ilGenImages(1, &imageID);
		ilBindImage(imageID);
		if (ilLoadImage(file.c_str())) {
			width = ilGetInteger(IL_IMAGE_WIDTH);
			height = ilGetInteger(IL_IMAGE_HEIGHT);
			initialized = true;
			textureID = ilutGLBindTexImage();
			ilDeleteImages(1, &imageID);
		}
		else {
			throw std::exception("Invalid sprite file");
		}
	}

}