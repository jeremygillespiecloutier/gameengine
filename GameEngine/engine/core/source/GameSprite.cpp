/*
Author: Jeremy Gillespie-Cloutier
Implementation of the GameSprite class
*/
#include <iostream>
#include <exception>
#include "GameSprite.h"
#include "GameRenderer.h"
using std::string;

namespace engine {

	GameSprite::GameSprite(string file, double x, double y, double width, double height) : GameTexture(file), x(x), y(y) {}

	void GameSprite::display() {
		GameRenderer::drawSprite(*this);
	}

	void GameSprite::update(unsigned deltaTime) {

	}

}