/*
Author: Jeremy Gillespie-Cloutier
Implementation ofthe  GameWindow class
*/
#include <iostream>
#include <exception>
#include <glew.h>
#include <freeglut.h>
#include <IL/il.h>
#include <IL/ilu.h>
#include <IL/ilut.h>
#include"GameWindow.h"
#include "GameRenderer.h"
#include "Rectangle.h"
#include "GameColor.h"
#include "GameInput.h"
#include "GameFont.h"
#include "Vector3.h"
using std::string;

namespace engine {

	GameWindow *GameWindow::window = nullptr;
	bool GameWindow::windowActive = false;
	int GameWindow::width = 0;
	int GameWindow::height = 0;

	GameWindow::GameWindow(int width, int height, string title) : title(title) {
		// If a window already exists, throw exception
		if (GameWindow::windowActive) {
			throw std::exception("Another game window is already active");
		}
		else if (width <= 0 || height <= 0) {
			throw std::exception("Window must have positive dimensions");
		}
		// Otherwise create a window
		GameWindow::windowActive = true;
		GameWindow::width = width;
		GameWindow::height = height;

		// Use dummy console arguments
		GameWindow::window = this;
		char arg[] = "arg";
		char *argv[] = { arg };
		int argc = 1;

		// Create the window
		glutInit(&argc, argv);
		glutInitWindowSize(width, height);
		glutCreateWindow(title.c_str());

		glutInitContextVersion(3, 1);

		// Set the otrhographic projection matrix such that lower left corner is (0,0) and top right corner is (width, height)
		glOrtho(0, width, 0, height, -1, 1);

		// Enable transparency
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_BLEND);

		glEnable(GL_DEPTH_TEST);

		// Initialize DevIL
		ilInit();
		iluInit();
		ilutRenderer(ILUT_OPENGL);

		// Initialize glew
		GLenum err = glewInit();
		if (GLEW_OK != err) {
			throw std::exception("Error initializing glew");
		}

		// Initialize renderer
		GameRenderer::initialize();

		// Initialize fonts
		GameFont::initialize();

		// Initialize the game input
		GameInput::initialize();

		// Enble shader buffers
		GLuint VertexArrayID;
		glGenVertexArrays(1, &VertexArrayID);
		glBindVertexArray(VertexArrayID);
	}

	// Initialize GLUT loop
	void GameWindow::start() {
		// Set callbacks
		glutDisplayFunc(GameWindow::displayFunc);
		glutKeyboardFunc(GameInput::keyboardFunc);
		glutSpecialFunc(GameInput::specialFunc);
		glutMouseFunc(GameInput::mouseFunc);
		glutPassiveMotionFunc(GameInput::passiveMotionFunc);
		glutIdleFunc(GameWindow::updateFunc);
		glutMainLoop();
	}

	// Display the scene if there is one
	void GameWindow::displayFunc() {
		GameScene *scene = GameWindow::window->scene;
		glClearColor(1, 1, 1, 1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glDisable(GL_TEXTURE_2D);
		// Display the scene
		if (scene != nullptr)
			scene->doDisplay();
		glFlush();
	}

	void GameWindow::updateFunc() {
		static int time = glutGet(GLUT_ELAPSED_TIME), lastUpdate = time;
		int newTime = glutGet(GLUT_ELAPSED_TIME);
		int diff = newTime - time;
		if (diff > 1000.0 / 30) {
			GameScene *scene = GameWindow::window->scene;
			scene->doUpdate(newTime - lastUpdate);
			lastUpdate = newTime;
			time += int(1000.0 / 30);
			glutPostRedisplay();
		}
	}

}