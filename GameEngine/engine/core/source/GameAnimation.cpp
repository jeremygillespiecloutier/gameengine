// TODO: Finish
/*
Author: Jeremy Gillespie-Cloutier
Implementation of the GameAnimation class
*/

#include "GameAnimation.h"
#include "GameRenderer.h"
#include <iostream>
#include <exception>

namespace engine {

	using std::string;

	// Creates an animation which uses the given files for animation frames.
	// Files: List of the file names to be loaded for each sprite frame
	GameAnimation::GameAnimation(std::initializer_list<std::string> files) {
		if (files.size() == 0)
			throw std::exception("An animation needs at least one image.");

		for (string file : files) {
			sprites.push_back(new GameSprite(file, 0, 0));
		}
		currentSprite = sprites[0];
		frameDuration = duration / sprites.size();
	}

	// Creates an animation using the given prefix and range of integers suffixes as file names.
	// Eg. with prefix="anim", extension="png", suffixStart=0, suffixStop=11 and digits=3
	// the animation will use anim_001.png, anim_002.png, ..., anim_011.png as sprite frames.
	// prefix: The file name suffix shared by all sprites in the animation
	// extension: The file extensions of the images in the animation
	// suffixStart: Suffix value for the first sprite in the animation
	// suffixStop: Suffix value for the last sprite in the animation
	// digits: number of digits included in the suffix, padded with 0s to the left. Must be large enough t4o fit the stop suffix value.
	GameAnimation::GameAnimation(std::string prefix, std::string extension, unsigned suffixStart, unsigned suffixStop, unsigned digits) {
		if (suffixStop <= suffixStart)
			throw std::exception("An animation needs at least one image.");
		if (std::to_string(suffixStop).length() > digits)
			throw std::exception("The stop suffix cannot have more digits than the given number of digits");

		for (unsigned i = suffixStart; i <= suffixStop; i++) {
			string fileName = std::to_string(i);
			unsigned diff = digits - fileName.length();
			for (unsigned j = 0; j < diff; j++)
				fileName = "0" + fileName;
			fileName = prefix + fileName + "." + extension;
			sprites.push_back(new GameSprite(fileName, 0, 0));
		}
		currentSprite = sprites[0];
		frameDuration = duration / sprites.size();
	}

	GameAnimation::~GameAnimation() {
		for (unsigned i = 0; i < sprites.size(); i++) {
			delete sprites[i];
		}
	}

	void GameAnimation::play() {
		paused = false;
	}

	void GameAnimation::pause() {
		paused = true;
	}

	void GameAnimation::setPosition(int x, int y) {
		for (GameSprite *sprite : sprites) {
			sprite->setPosition(x, y);
		}
	}

	void GameAnimation::setDuration(unsigned duration) {
		this->duration = duration;
	}

	void GameAnimation::setFrame(unsigned frame) {
		if (frame >= sprites.size())
			throw std::exception("Frame index out of bounds.");
		currentFrame = frame;
		counter = 0; // Also reset counter since we move to the beginning of the frame
	}

	void GameAnimation::update(unsigned deltaTime) {
		if (!paused) {
			counter += deltaTime;
			if (counter > frameDuration) {
				counter -= frameDuration;
				currentFrame = (currentFrame + 1) % sprites.size();
				counter = 0;
				currentSprite = sprites[currentFrame];
			}
		}
	}

	void GameAnimation::display() {
		GameRenderer::drawSprite(*currentSprite);
	}

}