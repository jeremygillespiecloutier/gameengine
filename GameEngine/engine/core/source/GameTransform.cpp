/*
Author: Jeremy Gillespie-Cloutier
Implementation of the game transform class
*/
#include "GameTransform.h"

namespace engine {

	// Creates a transform with the given position, rotation and scale
	GameTransform::GameTransform(Vector3 position, Vector3 rotation, Vector3 scale)
		: position(position), rotation(rotation), scale(scale) { }

	// Creates a transform with default position (0, 0, 0), default rotation (0, 0, 0) and default scale (1, 1, 1)
	GameTransform::GameTransform() : position(Vector3{ 0, 0, 0 }), rotation(Vector3{ 0, 0, 0 }), scale(Vector3{ 1, 1, 1 }) { }

	// Rotates the transform by the given x, y and z angles (in radians)
	void GameTransform::rotateBy(double rotationX, double rotationY, double rotationZ) {
		rotation.set(rotation.getX() + rotationX, rotation.getY() + rotationY, rotation.getZ() + rotationZ);
	}

	// Rotates the transform by the given rotation vector (with radian components)
	void GameTransform::rotateBy(const Vector3& rotation) {
		rotateBy(rotation.getX(), rotation.getY(), rotation.getZ());
	}

	// Scales the transform by the given x, y and z scale factors
	void GameTransform::scaleBy(double scaleX, double scaleY, double scaleZ) {
		scale.set(scale.getX() * scaleX, scale.getY() * scaleY, scale.getZ() * scaleZ);
	}

	// Scales the transform by the given scaling vector
	void GameTransform::scaleBy(const Vector3& scale) {
		scaleBy(scale.getX(), scale.getY(), scale.getZ());
	}

	// Translates the transform by the given x, y and z offsets
	void GameTransform::translateBy(double translationX, double translationY, double translationZ) {
		position.set(position.getX() + translationX, position.getY() + translationY, position.getZ() + translationZ);
	}

	// Translates the transform by the given translation vector
	void GameTransform::translateBy(const Vector3& translation) {
		translateBy(translation.getX(), translation.getY(), translation.getZ());
	}

	// Applies the transformation defined by this transform
	void GameTransform::apply() {
		// TODO: finish (perhaps switch to matrix based transform?)
		glTranslated(position.getX(), position.getY(), position.getZ());
		glRotated(rotation.getX(), 1, 0, 0);
		glRotated(rotation.getY(), 0, 1, 0);
		glRotated(rotation.getZ(), 0, 0, 1);
	}

}