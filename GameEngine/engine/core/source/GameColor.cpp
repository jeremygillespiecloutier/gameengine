/*
Author: Jeremy Gillespie-Cloutier
Implementation of the GameColor class
*/
#include "GameColor.h"

namespace engine {

	GameColor::GameColor() : GameColor(1, 1, 1, 1) {}
	GameColor::GameColor(double r, double g, double b, double a) : r(r), g(g), b(b), a(a) { }

	const GameColor GameColor::RED(1, 0, 0, 1);
	const GameColor GameColor::GREEN(0, 1, 0, 1);
	const GameColor GameColor::BLUE(0, 0, 1, 1);

	// Sets the r/g/b/a values of the color
	void GameColor::set(double r, double g, double b, double a) {
		this->r = r;
		this->g = g;
		this->b = b;
		this->a = a;
	}

}