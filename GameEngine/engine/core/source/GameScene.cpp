/*
Author: Jeremy Gillespie-Cloutier
Implementation of the GameScene class
*/
#include "GameScene.h"
#include "GameColor.h"
#include "GameRenderer.h"
#include <freeglut.h>

namespace engine {

	GameScene::GameScene() :camera(PerspectiveCamera()) {}

	void GameScene::addActor(GameActor *actor) {
		// TODO: Add checks to ensure the same actor is not added twice
		actors.push_back(actor);
	}

	bool GameScene::removeActor(GameActor *actor) {
		for (unsigned i = 0; i < actors.size();i++) {
			GameActor *child = actors[i];
			if (actor == child) {
				actors.erase(actors.begin() + i);
				return true;
			}
		}
		return false;
	}

	void GameScene::addLight(const GameLight *light) {
		// TODO: Add checks to ensure the same light is not added twice
		lights.push_back(light); 
	}

	bool GameScene::removeLight(const GameLight *light) {
		for (unsigned i = 0; i < lights.size(); i++) {
			const GameLight *otherLight = lights[i];
			if (light == otherLight) {
				lights.erase(lights.begin() + i);
				return true;
			}
		}
		return false;
	}

	void GameScene::doDisplay() {
		GameRenderer::clearBuffers();
		glPushMatrix();
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		for (GameShader *shader : shaders) {
			shader->setLights(lights);
		}
		for (GameActor *actor : actors) {
			actor->doDisplay();
		}
		display();
		glPopMatrix();
	}

	void GameScene::doUpdate(unsigned deltaTime) {
		for (GameActor *actor : actors) {
			actor->doUpdate(deltaTime);
		}
		update(deltaTime);
	}

	void GameScene::setCamera(const GameCamera &camera) {
		this->camera = camera;
		camera.apply();
	}

	void GameScene::attachShader(GameShader* shader) {
		// TODO: Add checks to ensure the same shader is not added twice
		shaders.push_back(shader);
	}

	bool GameScene::detachShader(GameShader *shader) {
		for (unsigned i = 0; i < shaders.size(); i++) {
			GameShader *otherShader = shaders[i];
			if (otherShader == shader) {
				lights.erase(lights.begin() + i);
				return true;
			}
		}
		return false;
	}

}