/*
Author: Jeremy Gillespie-Cloutier
Implementation of the GameActor class
*/
#include "GameActor.h"
#include <exception>
#include "GameRenderer.h"

namespace engine {

	void GameActor::addComponent(GameComponent &component) {
		if (component.actor == nullptr) {
			components.push_back(&component);
			component.actor = this;
		}
		else
			throw std::exception("This component has already been added to an actor");
	}

	void GameActor::removeComponent(GameComponent &component) {
		for (unsigned i = 0; i < components.size(); i++) {
			GameComponent *comp = components[i];
			if (comp == &component) {
				components.erase(components.begin() + i);
				component.actor = nullptr;
				return;
			}
		}
	}

	void GameActor::addChild(GameActor &child) {
		children.push_back(&child);
	}

	void GameActor::removeChild(GameActor &child) {
		for (unsigned i = 0; i < children.size(); i++) {
			GameActor *actor = children[i];
			if (actor == &child) {
				children.erase(children.begin() + i);
				return;
			}
		}
	}

	// Diplays the actor, its components and its children
	void GameActor::doDisplay() {

		GameShader *oldShader = GameRenderer::shader;
		GameRenderer::setShader(shader);

		this->camera->apply();
		glMatrixMode(GL_MODELVIEW);

		glPushMatrix();

		transform.apply(); // Apply the actor transformation matrix

		for (GameComponent *comp : components) {
			comp->display();
		}
		display();

		for (GameActor *child : children) {
			child->doDisplay();
		}

		glPopMatrix();

		GameRenderer::setShader(oldShader);
	}

	// Updates the actor, its components and its children
	void GameActor::doUpdate(unsigned deltaTime) {
		for (GameComponent *comp : components) {
			comp->update(deltaTime);
		}

		for (GameActor *child : children) {
			child->doUpdate(deltaTime);
		}
		update(deltaTime);
	}

	void GameActor::setCamera(GameCamera *camera) {
		this->camera = camera;
	}

	GameCamera* GameActor::getCamera() {
		return this->camera;
	}

	void GameActor::setShader(GameShader *shader) {
		GameActor::shader = shader;
	}

}