/*
Author: Jeremy Gillespie-Cloutier
Implementation of the GameInput class
*/
#include "GameInput.h"
#include <iostream>
#include <string>
#include "GameWindow.h"

namespace engine {

	bool GameInput::keysDown[NO_KEY];
	bool GameInput::keysJustDown[NO_KEY];
	int GameInput::mouseX = 0;
	int GameInput::mouseY = 0;
	std::map<unsigned char, int> GameInput::keyMap = { {'a',KeyCode::A} };


	void GameInput::initialize() {
		for (bool &val : GameInput::keysDown)
			val = false;
		for (bool &val : GameInput::keysJustDown)
			val = false;
	}

	void GameInput::keyboardFunc(unsigned char key, int x, int y) {
		if (keyMap.count(key) != 0)
			std::cout << "PRESSED " << key << std::endl;
	}

	void GameInput::specialFunc(int key, int x, int y) {

	}

	void GameInput::mouseFunc(int button, int state, int x, int y) {

	}

	void GameInput::passiveMotionFunc(int x, int y) {
		mouseX = x;
		mouseY = GameWindow::getHeight() - y;
	}

	void GameInput::update() {

	}

	bool  GameInput::isMouseDown(int button) {
		return false;
	}

	bool  GameInput::isMouseJustDown(int button) {
		return false;
	}

	bool  GameInput::isKeyDown(KeyCode code) {
		return false;
	}

	bool  GameInput::isKeyJustDown(KeyCode code) {
		return false;
	}

}