/*
Author: Jeremy Gillespie-Cloutier
Implementation of the GameRenderer class
*/
#include "GameRenderer.h"
#include <iostream>
#define PI 3.14159265359

namespace engine {

	GameColor GameRenderer::color(1, 1, 1, 1);
	Vector3 GameRenderer::textureCoord{0, 0, 0};
	float GameRenderer::lineWidth = 1;
	GameShader *GameRenderer::shader = nullptr;
	GLuint GameRenderer::samplerID = 0;

	std::vector<double> GameRenderer::vertexBuffer{};
	std::vector<double> GameRenderer::colorBuffer{};
	std::vector<double> GameRenderer::textureBuffer{};
	std::vector<double> GameRenderer::normalBuffer{};
	GLuint GameRenderer::vertexBufferID = -1;
	GLuint GameRenderer::colorBufferID = -1;
	GLuint GameRenderer::textureBufferID = -1;
	GLuint GameRenderer::normalBufferID = -1;

	Vector3 GameRenderer::normal{ 0,0,0 };

	bool GameRenderer::renderActive = false;
	RenderType GameRenderer::renderType = RenderType::Triangles;

	// Initializes the data buffers
	void GameRenderer::initialize() {
		glGenBuffers(1, &GameRenderer::vertexBufferID);
		glGenBuffers(1, &GameRenderer::colorBufferID);
		glGenBuffers(1, &GameRenderer::textureBufferID);
		glGenBuffers(1, &GameRenderer::normalBufferID);

		glGenSamplers(1, &samplerID);
		glSamplerParameteri(samplerID, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(samplerID, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glSamplerParameteri(samplerID, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glSamplerParameteri(samplerID, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glSamplerParameterf(samplerID, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16.0f);
	}

	// Begins the rendering phase using the given render type
	// type: The render type to use when rendering
	void GameRenderer::beginRender(RenderType type) {
		if (!renderActive) {
			renderActive = true;
			renderType = type;
			vertexBuffer.clear();
			colorBuffer.clear();
			textureBuffer.clear();
			normalBuffer.clear();
		}
	}

	// Stops the rendering phase
	void GameRenderer::endRender() {
		if (renderActive) {
			renderActive = false;
			if (shader != nullptr) {
				shader->doApply();
			}
		}
	}

	// Clears all data buffers and stops the current render phase
	void GameRenderer::clearBuffers() {
		vertexBuffer.clear();
		colorBuffer.clear();
		textureBuffer.clear();
		normalBuffer.clear();
		renderActive = false;
	}

	// Adds a new vertex to the vertex buffer
	// x: The vertex x component
	// y: The vertex y component
	// z: The vertex z component
	void GameRenderer::pushVertex(double x, double y, double z) {
		if (renderActive) {
			vertexBuffer.push_back(x);
			vertexBuffer.push_back(y);
			vertexBuffer.push_back(z);

			colorBuffer.push_back(color.getR());
			colorBuffer.push_back(color.getG());
			colorBuffer.push_back(color.getB());
			colorBuffer.push_back(color.getA());

			textureBuffer.push_back(textureCoord.getX());
			textureBuffer.push_back(textureCoord.getY());

			normalBuffer.push_back(normal.getX());
			normalBuffer.push_back(normal.getY());
			normalBuffer.push_back(normal.getZ());
		}
	}

	// Adds a new vertex to the vertex buffer
	// vertex: The vertex to add
	void GameRenderer::pushVertex(Vector3 vertex) {
		pushVertex(vertex.getX(), vertex.getY(), vertex.getZ());
	}

	// Sets the texture coordinate that will be passed to the vertex shader for subsequent vertices 
	// x: The texture coord x component
	// y: The texture coord y component
	void GameRenderer::setTextureCoord(double x, double y) {
		textureCoord = Vector3{ x, y, 0 };
	}

	// Sets the texture coordinate that will be passed to the vertex shader for subsequent vertices 
	// Note: Calling this will disable colors for the next vertices (until setColor is called, see GameRenderer::setColor)
	// coord: The texture coordinate to add
	void GameRenderer::setTextureCoord(Vector3 coord) {
		setTextureCoord(coord.getX(), coord.getY());
	}

	// Sets the color that will be passed to the vertex shader for subsequent vertices 
	void GameRenderer::setColor(double r, double g, double b, double a) {
		color.set(r, g, b, a);
	}

	// Sets the color that will be passed to the vertex shader for subsequent vertices 
	// Note: Calling this will disable textures for the next vertices (until setTextureCoord is called, see GameRenderer::setTextureCoord)
	void GameRenderer::setColor(GameColor col) {
		setColor(col.getR(), col.getG(), col.getB(), col.getA());
	}

	void GameRenderer::setNormal(double x, double y, double z) {
		normal.set(x, y, z);
	}

	void GameRenderer::setNormal(Vector3 normal) {
		setNormal(normal.getX(), normal.getY(), normal.getZ());
	}

	void GameRenderer::drawRectangle(double x, double y, double width, double height) {
		glLineWidth(lineWidth);

		double halfWidth = width / 2, halfHeight = height / 2;
		GameRenderer::beginRender(RenderType::LineLoop);
		GameRenderer::pushVertex(x - halfWidth, y - halfHeight);
		GameRenderer::pushVertex(x + halfWidth, y - halfHeight);
		GameRenderer::pushVertex(x + halfWidth, y + halfHeight);
		GameRenderer::pushVertex(x - halfWidth, y + halfHeight);
		GameRenderer::endRender();
	}

	void GameRenderer::drawRectangle(const Rectangle &rec) {
		drawPolygon(rec);
	}

	void GameRenderer::drawLine(double x1, double y1, double x2, double y2) {
		glLineWidth(lineWidth);

		GameRenderer::beginRender(RenderType::Lines);
		GameRenderer::pushVertex(x1, y1);
		GameRenderer::pushVertex(x2, y2);
		GameRenderer::endRender();
	}

	void GameRenderer::drawBezier(const BezierCurve &curve, unsigned points) {
		glLineWidth(lineWidth);

		GameRenderer::beginRender(RenderType::LineStrip);
		double step = 1.0 / points;
		for (double i = 0; i < 1; i += step) {
			Vector3 current = curve.pointAt(i);
			GameRenderer::pushVertex(current.getX(), current.getY());
		}
		Vector3 last = curve.pointAt(1);
		GameRenderer::pushVertex(last.getX(), last.getY());
		GameRenderer::endRender();
	}

	void GameRenderer::fillRectangle(double x, double y, double width, double height) {
		double halfWidth = width / 2, halfHeight = height / 2;
		GameRenderer::beginRender(RenderType::Quads);
		GameRenderer::pushVertex(x - halfWidth, y - halfHeight);
		GameRenderer::pushVertex(x + halfWidth, y - halfHeight);
		GameRenderer::pushVertex(x + halfWidth, y + halfHeight);
		GameRenderer::pushVertex(x - halfWidth, y + halfHeight);
		GameRenderer::endRender();
	}

	void GameRenderer::fillRectangle(const Rectangle &rec) {
		std::vector<Vector3> points = rec.getPoints();
		GameRenderer::beginRender(RenderType::Quads);
		GameRenderer::pushVertex(points[0].getX(), points[0].getY());
		GameRenderer::pushVertex(points[1].getX(), points[1].getY());
		GameRenderer::pushVertex(points[2].getX(), points[2].getY());
		GameRenderer::pushVertex(points[3].getX(), points[3].getY());
		GameRenderer::endRender();
	}

	void GameRenderer::fillTriangle(double x1, double y1, double x2, double y2, double x3, double y3) {
		GameRenderer::beginRender(RenderType::Triangles);
		GameRenderer::pushVertex(x1, y1, 0);
		GameRenderer::pushVertex(x2, y2, 0);
		GameRenderer::pushVertex(x3, y3, 0);
		GameRenderer::endRender();
	}

	void GameRenderer::fillEllipse(double x, double y, double width, double height, int numTriangles) {
		width /= 2;
		height /= 2;
		GameRenderer::beginRender(RenderType::TriangleFan);
		GameRenderer::pushVertex(x, y);
		for (int curr = 0; curr <= numTriangles; curr++) {
			double t = static_cast<double>(curr) / numTriangles * 2 * PI;
			GameRenderer::pushVertex(x + width * cos(t), y + height * sin(t));
		}
		GameRenderer::endRender();
	}

	void GameRenderer::drawEllipse(double x, double y, double width, double height, int segments) {
		glLineWidth(lineWidth);

		width /= 2;
		height /= 2;
		GameRenderer::beginRender(RenderType::LineLoop);
		for (int curr = 0; curr <= segments; curr++) {
			double t = static_cast<double>(curr) / segments * 2 * PI;
			GameRenderer::pushVertex(x + width * cos(t), y + height * sin(t));
		}
		GameRenderer::endRender();
	}

	void GameRenderer::drawPolygon(const Polygon2D &polygon) {
		glLineWidth(lineWidth);

		GameRenderer::beginRender(RenderType::LineLoop);
		for (const Vector3 &p : polygon.getPoints())
			GameRenderer::pushVertex(p.getX(), p.getY());
		GameRenderer::endRender();
	}

	void GameRenderer::drawSprite(const GameSprite &sprite) {
		glEnable(GL_TEXTURE_2D);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, sprite.getTextureID());
		glBindSampler(sprite.getTextureID(), samplerID);

		glPushMatrix();
		glTranslated(sprite.getX(), sprite.getY(), 0);

		GameRenderer::beginRender(RenderType::Quads);
		GameRenderer::setTextureCoord(0, 0); GameRenderer::pushVertex(0, 0);
		GameRenderer::setTextureCoord(0, 1); GameRenderer::pushVertex(0, (float)sprite.getHeight());
		GameRenderer::setTextureCoord(1, 1); GameRenderer::pushVertex((float)sprite.getWidth(), (float)sprite.getHeight());
		GameRenderer::setTextureCoord(1, 0); GameRenderer::pushVertex((float)sprite.getWidth(), 0.0);
		GameRenderer::endRender();

		glPopMatrix();

		glDisable(GL_TEXTURE_2D);
	}

	void GameRenderer::useTexture(const GameTexture &texture) {
		glEnable(GL_TEXTURE_2D);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture.getTextureID());
		glBindSampler(texture.getTextureID(), samplerID);
	}

}