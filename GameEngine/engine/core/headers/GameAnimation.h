// TODO: Finish
/*
Author: Jeremy Gillespie-Cloutier
Class that handles the animation of sprites
*/

#ifndef H_GAMEANIMATION
#define H_GAMEANIMATION

#include "GameSprite.h"
#include "GameComponent.h"
#include <initializer_list>
#include <string>
#include <vector>

namespace engine {

	class GameAnimation : public GameComponent {
	public:
		// Create a game animation from a list of image files
		GameAnimation(std::initializer_list<std::string> files);
		// Creates an animation with files (prefix)(suffixStart).(extension) to (prefix)(suffixStop).(extension)
		// Where digits is the length of the sufix (eg. 3 for suffixes 000, 001, 002 ...)
		GameAnimation(std::string prefix, std::string extension, unsigned suffixStart, unsigned suffixStop, unsigned digits);
		virtual ~GameAnimation();
		void play();
		void pause();
		void setDuration(unsigned  duration);
		void setFrame(unsigned frame);
		void setPosition(int x, int y);
	protected:
		void update(unsigned deltaTime) override;
		void display() override;
	private:
		unsigned duration = 500; // Time to complete the animation in milliseconds
		unsigned frameDuration; // Duration of each animation frame
		unsigned currentFrame = 0; // Current animation frame
		unsigned counter = 0; // The delta time animation counter
		bool paused = true;
		std::vector<GameSprite*> sprites;
		GameSprite *currentSprite;
	};

}

#endif