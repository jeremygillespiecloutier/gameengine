#ifndef H_GAMELIGHT
#define H_GAMELIGHT
#include "GameColor.h"
#include "Vector3.h"

namespace engine {

	enum LightType {
		Directional,
		Point
	};

	class GameLight {
	public:
		inline LightType getType() const { return type; }
		inline GameColor getColor() const { return color; }
		inline Vector3 getPosition() const { return position; }
		inline Vector3 getDirection() const { return direction; }
	protected:
		GameLight(GameColor color, LightType type);
		GameColor color;
		LightType type;
		Vector3 position;
		Vector3 direction;
	};

	class DirectionalLight : public GameLight {
	public:
		DirectionalLight(Vector3 direction, GameColor color);
	};

	class PointLight : public GameLight {
	public:
		PointLight(Vector3 position, GameColor color);
	};

}

#endif