#ifndef H_GAMEMESH
#define H_GAMEMESH

#include "Vector3.h"
#include "GameComponent.h"
#include <vector>
#include <string>
#include <map>
#include "GameMaterial.h"

namespace engine {

	class MeshFace {
	public:
		unsigned vertices[3] = { 0, 0, 0 };
		unsigned vertexNormals[3] = { 0, 0, 0 };
		unsigned textureVertices[3] = { 0, 0, 0 };
		bool hasNormals = false;
		bool hasTexture = false;
		bool hasMaterial = false;
		std::string materialName = "";
	};

	class GameMesh : public GameComponent {
	public:
		GameMesh(std::vector<Vector3> vertices, std::vector<Vector3> vertexNormals,
			std::vector<MeshFace> faces, std::vector<Vector3> textureVertices, std::map<std::string, GameMaterial> materials);
	private:
		void display();
		void update(unsigned deltaTime);
		std::vector<Vector3> vertices;
		std::vector<Vector3> vertexNormals;
		std::vector<Vector3> textureVertices;
		std::vector<MeshFace> faces;
		std::map<std::string, GameMaterial> materials{};
	};

}

#endif