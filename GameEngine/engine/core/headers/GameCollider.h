/*
TODO: Finish
Author: Jeremy Gillespie-Cloutier
GameCollider component that can be attached to actor to detect collisions
*/
#ifndef H_GAMECOLLIDER
#define H_GAMECOLLIDER

#include "GameComponent.h"
#include "Shape.h"

namespace engine {

	class GameCollider : public GameComponent {
	public:
		GameCollider(const Shape &shape);
	protected:
		void update(unsigned deltaTime) override;
		void display() override;
	};

}

#endif