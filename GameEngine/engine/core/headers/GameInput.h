/*
Author: Jeremy Gillespie-Cloutier
This class receives and handles the input events from the game
*/
#ifndef H_GAMEINPUT
#define H_GAMEINPUT
#include <map>
#include "Vector3.h"

namespace engine {

	class GameWindow;
	enum KeyCode {
		W, A, S, D, NO_KEY
	};

	class GameInput {
		friend class GameWindow;
	public:
		static bool isMouseDown(int button);
		static bool isMouseJustDown(int button);
		static bool isKeyDown(KeyCode code); // Determines if the given key is currently pressed
		static bool isKeyJustDown(KeyCode code); // Determines if the given key was pressed this frame
		static int getMouseX() { return mouseX; }
		static int getMouseY() { return mouseY; }
		static Vector3 getMousePosition() { return Vector3(mouseX, mouseY, 0); }
	private:
		static std::map<unsigned char, int> keyMap;
		static int mouseX, mouseY;
		static bool keysDown[];
		static bool keysJustDown[];
		static void initialize();
		// Functions called by GLUT
		static void keyboardFunc(unsigned char key, int x, int y);
		static void specialFunc(int key, int x, int y);
		static void mouseFunc(int button, int state, int x, int y);
		static void passiveMotionFunc(int x, int y);
		static void update();
	};

}

#endif
