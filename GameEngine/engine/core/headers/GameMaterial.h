#ifndef H_GAMEMATERIAL
#define H_GAMEMATERIAL

#include "GameColor.h"
#include "GameTexture.h"

namespace engine {

	class GameMaterial {
	public:
		GameMaterial() {};
		void setAmbientColor(GameColor ambientColor);
		void setDiffuseColor(GameColor diffuseColor);
		void setEmissiveColor(GameColor emissiveColor);
		void setSpecularColor(GameColor specularColor);
		void setAlpha(double alpha);
		void setFlatLighting(bool flatLighting);
		void setShine(double shine);
		void setTexture(std::string file);
		const GameTexture& getTexture() { return texture; };
	protected:
		GameColor ambientColor;
		GameColor diffuseColor;
		GameColor emissiveColor;
		GameColor specularColor;
		GameTexture texture;
		double alpha;
		double shine;
		bool flatLighting;
	};

}

#endif