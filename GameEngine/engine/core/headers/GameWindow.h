/*
Author: Jeremy Gillespie-CLoutier
Class that defines a game window. Only one instance can be active at a time
*/

#ifndef H_GAMEWINDOW
#define H_GAMEWINDOW

#include <string>
#include "GameColor.h"
#include "GameScene.h"
#include <vector>

namespace engine {

	class GameWindow {
	public:
		GameWindow(int width, int height, std::string title);
		inline void setScene(GameScene &scene) { this->scene = &scene; } // Set the current scene displayed
		void start(); // Function called to start the main game and display loop on the window
		static int getWidth() { return width; }
		static int getHeight() { return height; }
	private:
		static int width, height; // Width and height of the active game window
		std::string title; // Title of the window
		GameScene *scene = nullptr; // The current scene being displayed
		static GameWindow *window; // Pointer to the current window
		static bool windowActive; // Flag indicating if a window has already been created
		static void displayFunc(); // GLUT display function
		static void updateFunc(); // GLUT update (idle) function
	};

}

#endif