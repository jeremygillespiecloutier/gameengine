/*
Author: Jeremy Gillespie-Cloutier
Class that loads and renders fonts
*/
#ifndef H_GAMEFONT
#define H_GAMEFONT

#include "ft2build.h"
#include FT_FREETYPE_H  

namespace engine {

	class GameFont {
	public:
		friend class GameWindow;
	protected:
		static void initialize();
		static FT_Library library;
	};

}

#endif