/*
Author: Jeremy Gillespie-Cloutier
GameScene class that represents what is seen on the screen. A scene is comprised of multiple actors
*/
#ifndef H_GAMESCENE
#define H_GAMESCENE

#include <vector>
#include "GameActor.h"
#include "GameCamera.h"
#include "GameLight.h"
#include "GameShader.h"

namespace engine {

	class GameScene {
		friend class GameWindow;
	public:
		GameScene();
		void addActor(GameActor *actor);
		bool removeActor(GameActor *actor);
		void addLight(const GameLight *light);
		bool removeLight(const GameLight *light);
		void attachShader(GameShader* shader);
		bool detachShader(GameShader *shader);
		virtual ~GameScene() = default;

		void setCamera(const GameCamera &camera);
	protected:
		virtual void display() = 0;
		virtual void update(unsigned deltaTime) = 0;
	private:
		GameCamera &camera;
		std::vector<GameActor*> actors;
		std::vector<const GameLight*> lights;
		std::vector<GameShader*> shaders;
		void doDisplay();
		void doUpdate(unsigned deltaTime);
	};
}

#endif
