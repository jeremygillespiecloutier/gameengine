/*
Author: Jeremy Gillespie-Cloutier
Base class for game components that can be added to actors
*/
#ifndef H_GAMECOMPONENT
#define H_GAMECOMPONENT

namespace engine {

	class GameComponent {
		friend class GameActor;
	public:
		virtual ~GameComponent() = default;
	protected:
		virtual void update(unsigned deltaTime) = 0;
		virtual void display() = 0;
		GameActor *actor = nullptr;
	};

}

#endif