/*
Author: Jeremy Gillespie-Cloutier
Class that represents an rgba on-screen color
*/
#ifndef H_GAMECOLOR
#define H_GAMECOLOR

namespace engine {
	class GameColor {
	public:
		GameColor();
		GameColor(double r, double g, double b, double a = 1);

		inline double getR() const { return r; }
		inline double getG() const { return g; }
		inline double getB() const { return b; }
		inline double getA() const { return a; }

		void set(double r, double g, double b, double a = 1);

		static const GameColor RED, GREEN, BLUE;
	private:
		double r, g, b, a;
	};
}

#endif
