/*
Author: Jeremy Gillespie-Cloutier
Class to display various shapes and sprites on screen.
*/
#ifndef H_GAMERENDERER
#define H_GAMERENDERER

#include "Rectangle.h"
#include "GameColor.h"
#include "GameSprite.h"
#include "Vector3.h"
#include "BezierCurve.h"
#include "GameShader.h"

namespace engine {

	enum RenderType {
		Points = GL_POINTS,
		Lines = GL_LINES,
		LineStrip = GL_LINE_STRIP,
		LineLoop = GL_LINE_LOOP,
		Triangles = GL_TRIANGLES,
		TriangleStrip = GL_TRIANGLE_STRIP,
		TriangleFan = GL_TRIANGLE_FAN,
		Quads = GL_QUADS,
		QuadStrip = GL_QUAD_STRIP,
		Polygon = GL_POLYGON
	};

	class GameRenderer {
	public:
		friend class GameScene;
		friend class GameShader;
		friend class GameWindow;
		friend class GameActor;

		// Draw a line on screen
		static void drawLine(double x1, double y1, double x2, double y2);

		// Draw a bezier curve on screen (approximated with the given number of points)
		static void drawBezier(const BezierCurve &curve, unsigned points);

		// Draw a rectangle on screen
		static void drawRectangle(const Rectangle&);
		static void drawRectangle(double x, double y, double width, double height);

		// Fill a rectangle on screen
		static void fillRectangle(const Rectangle&);
		static void fillRectangle(double x, double y, double width, double height);

		// Draw a polygon
		static void drawPolygon(const Polygon2D &polygon);

		// Fill a triangle
		static void fillTriangle(double x1, double y1, double x2, double y2, double x3, double y3);

		// Fill an ellipse on screen with the given number of triangles
		static void fillEllipse(double x, double y, double width, double height, int numTriangles);

		// Draw an ellipse on screen with the given number of segments
		static void drawEllipse(double x, double y, double width, double height, int segments);

		// Render a sprite on screen
		static void drawSprite(const GameSprite &sprite);

		static void setColor(GameColor col);
		static void setColor(double r, double g, double b, double a = 1);

		static inline void setLineWidth(float lineWidth) { GameRenderer::lineWidth = lineWidth; } // Set the line width

		static inline void setShader(GameShader *shader) { GameRenderer::shader = shader; }

		static void useTexture(const GameTexture &texture);

		static void pushVertex(Vector3 vertex);
		static void pushVertex(double x, double y, double z = 0);
		static void setTextureCoord(Vector3 coord);
		static void setTextureCoord(double u, double v);
		static void beginRender(RenderType type);
		static void endRender();
		static void setNormal(double x, double y, double z=0);
		static void setNormal(Vector3 normal);

		static const inline std::vector<double> & getVertexBuffer() { return vertexBuffer; }
		static const inline std::vector<double> & getColorBuffer() { return colorBuffer; }
		static const inline std::vector<double> & getTextureBuffer() { return textureBuffer; }
		static const inline std::vector<double> & getNormalBuffer() { return normalBuffer; }

		static const inline GLuint getVertexBufferID() { return vertexBufferID; }
		static const inline GLuint getColorBufferID() { return colorBufferID; }
		static const inline GLuint getTextureBufferID() { return textureBufferID; }
		static const inline GLuint getNormalBufferID() { return normalBufferID; }

		static const inline RenderType getRenderType() { return renderType; }

	private:
		static GameColor color; // The rendering color for the current vertex
		static float lineWidth; // The width of lines drawn
		static Vector3 textureCoord; // The texture coordinate for the current vertex
		static bool colorActive; // Indicates if colors are active (false = textures active)

		static void initialize();

		static std::vector<double> vertexBuffer; // Buffer containing the vertices that will be sent to the shader
		static std::vector<double> colorBuffer; // Buffer containing the colors that will be sent to the shader
		static std::vector<double> textureBuffer; // Buffer containing the texture coordinates that will be sent to the shader
		static std::vector<double> normalBuffer; // Buffer containing the vertex normals that will be sent to the shader

		static GLuint vertexBufferID;
		static GLuint colorBufferID;
		static GLuint textureBufferID;
		static GLuint normalBufferID;

		static GLuint samplerID;

		static Vector3 normal;

		static GameShader *shader; // Shader that will render elements

		static RenderType renderType; // How the current vertex buffer will be renderer
		static bool renderActive; // Flag indicating if the renderer is currently rendering vertices

		static void clearBuffers();
	};

}

#endif