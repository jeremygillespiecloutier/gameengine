/*
Author: Jeremy Gillespie-Cloutier
Class that represents a game actor that lives inside a game scene
*/
#ifndef H_GAMEACTOR
#define H_GAMEACTOR

#include "Vector3.h"
#include "GameComponent.h"
#include <vector>
#include "GameTransform.h"
#include "GameCamera.h"
#include "GameShader.h"

namespace engine {

	class GameActor {
	public:
		friend class GameScene;

		virtual ~GameActor() = default;

		void addComponent(GameComponent &component);
		void removeComponent(GameComponent &component);

		void addChild(GameActor &child);
		void removeChild(GameActor &child);

		GameTransform& getTransform() { return transform; }

		void setCamera(GameCamera *camera);
		GameCamera* getCamera();

		void setShader(GameShader *shader);
	protected:
		virtual void display() = 0; // Called when display is being updated
		virtual void update(unsigned deltaTime) = 0; // Update loop

		GameTransform transform;
		std::vector<GameActor*> children;
		std::vector<GameComponent*> components{};
		GameCamera *camera;
		GameShader *shader = nullptr;
	private:
		void doDisplay();
		void doUpdate(unsigned deltaTime);
	};

}

#endif