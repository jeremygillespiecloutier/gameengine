/*
Author: Jeremy Gillespie-Cloutier
Class that allows to load and activate custom glsl shaders
*/
#ifndef H_GAMESHADER
#define H_GAMESHADER

#include <glew.h>
#include <string>
#include <vector>
#include <freeglut.h>
#include "Vector3.h"
#include "Matrix.h"
#include "GameLight.h"

namespace engine {

	class GameShader {
	public:
		friend class GameRenderer;
		friend class GameScene;
		GameShader(std::string vertexPath, std::string fragmentPath, std::vector<std::string> defineList = std::vector<std::string>());
		void setUniform(std::string uniform, float value);
		void setUniform(std::string uniform, std::vector<float> values);
		void setUniform(std::string uniform, int value);
		void setUniform(std::string uniform, std::vector<int> values);
		void setUniform(std::string uniform, Vector3 value);
		void setUniform(std::string uniform, std::vector<Vector3> values);
		void setUniform(std::string uniform, FixedMatrix<4, 4> value);
	protected:
		GLuint loadShader(std::string path, bool isVertex);
		GLuint vertexShader;
		GLuint fragmentShader;
		GLuint program;
		virtual void apply() = 0;
		void setLights(std::vector<const GameLight*> lights);
		std::vector<const GameLight*> lights{};
	private:
		void doApply();
		std::vector<std::string> defineList;
	};

	class ColorShader : public GameShader {
	public:
		ColorShader(std::string vertexPath, std::string fragmentPath, std::vector<std::string> defineList = std::vector<std::string>());
	protected:
		virtual void apply();
		bool lightingEnabled = false;
	};

	class TextureShader : public GameShader {
	public:
		TextureShader(std::string vertexPath, std::string fragmentPath, std::vector<std::string> defineList = std::vector<std::string>());
	protected:
		GLuint samplerID;
		virtual void apply();
	};
}

#endif