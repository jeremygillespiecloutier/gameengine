#ifndef H_GAMETEXTURE
#define H_GAMETEXTURE

#include <glew.h>
#include <freeglut.h>
#include <IL/il.h>
#include <IL/ilu.h>
#include <IL/ilut.h>
#include <string>

namespace engine {

	class GameTexture {
	public:
		GameTexture() {};
		GameTexture(std::string file);
		inline GLuint getTextureID() const { return textureID; }
		inline bool isInitialized() const { return initialized; }
		inline double getWidth() const { return width; }
		inline double getHeight() const { return height; }
	private:
		double width, height;
		ILuint imageID = 0;
		GLuint textureID = 0;
		bool initialized = false;
	};

}

#endif