/*
Author: Jeremy Gillespie-Cloutier
Class that models perspective and orthographic cameras and their transformations
*/
#ifndef H_GAMECAMERA
#define H_GAMECAMERA

#include "Vector3.h"
#include "Rectangle.h"

namespace engine {

	class ViewArea {
	public:
		ViewArea(double x, double y, double width, double height) : x(x), y(y), width(width), height(height) {};
		double getX() const { return x; }
		double getY() const { return y; }
		double getWidth() const { return width; }
		double getHeight() const { return height; }
	protected:
		double x, y, width, height;
	};

	class GameCamera {
	protected:
		friend class GameScene;
		friend class GameActor;
		GameCamera(ViewArea area, double leftDist, double rightDist, double bottomDist, double topDist, double nearDist, double farDist);
		virtual ~GameCamera() = default;
		//void lookAt(const Vector3 &position);
		//void lookAt(double x, double y, double z);

		virtual void apply() const {};
	protected:
		double leftDist, rightDist, bottomDist, topDist, nearDist, farDist;
		ViewArea area;
	};

	class PerspectiveCamera : public GameCamera {
	public:
		PerspectiveCamera();
		PerspectiveCamera(ViewArea area, double leftDist, double rightDist, double bottomDist, double topDist, double nearDist, double farDist);
	protected:
		void apply() const;
	};

	class OrtographicCamera : public GameCamera {
	public:
		OrtographicCamera(ViewArea area, double width, double height, double nearDist = 0, double farDist = 0);
	protected:
		void apply() const;
	};

}

#endif