/*
Author: Jeremy Gillespie-Cloutier
A GameSprite is an instance of an image on screen
*/
#ifndef H_GAMESPRITE
#define H_GAMESPRITE

#include "GameComponent.h"
#include <string>
#include <glew.h>
#include <freeglut.h>
#include <IL/il.h>
#include <IL/ilu.h>
#include <IL/ilut.h>
#include "GameTexture.h"

namespace engine {

	class GameSprite : public GameComponent, public GameTexture {
	public:
		GameSprite(std::string file, double x, double y, double width = -1, double height = -1);
		inline double getX() const { return x; }
		inline double getY() const { return y; }
		inline void setPosition(double x, double y) {
			this->x = x;
			this->y = y;
		}
	protected:
		void display() override;
		void update(unsigned deltaTime) override;
	private:
		double x = 0, y = 0;
	};

}

#endif