/*
Author: Jeremy Gillespie-Cloutier
A game transform stores, alters and applies the rotation, scale and position of a game actor
*/
#ifndef H_GAMETRANSFORM
#define H_GAMETRANSFORM

#include "Vector3.h"
#include <glew.h>
#include <freeglut.h>

namespace engine {

	class GameTransform {
	public:
		GameTransform();
		GameTransform(Vector3 position, Vector3 rotation, Vector3 scale);

		void rotateBy(const Vector3& rotation);
		void rotateBy(double rotationX, double rotationY, double rotationZ);

		void scaleBy(const Vector3& scale);
		void scaleBy(double scaleX, double scaleY, double scaleZ);

		void translateBy(const Vector3& translation);
		void translateBy(double translationX, double translationY, double translationZ);

		const Vector3& getScale() { return scale; }
		const Vector3& getRotation() { return rotation; }
		const Vector3& getPosition() { return position; }

		void apply();
	private:
		Vector3 rotation;
		Vector3 scale;
		Vector3 position;
	};
}

#endif