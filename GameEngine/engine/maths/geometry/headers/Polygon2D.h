/*
Author: Jeremy Gillespie-Cloutier
Represents a polygon by a sequence of points (vectors) in order
*/
#ifndef H_POLYGON
#define H_POLYGON
#include <vector>
#include "Vector3.h"
#include "Shape.h"
#include <initializer_list>

namespace engine {

	class Polygon2D : public Shape {
	public:
		Polygon2D(std::initializer_list<Vector3>);
		Polygon2D(std::vector<Vector3>);

		const inline std::vector<Vector3>& getPoints() const { return points; }

		const inline std::vector<Vector3>& getNormals() const { return normals; }

		const inline bool isClockwise() const { return clockwise; }

		const inline bool isConcave() const { return concave; }

		void rotate(const Vector3 &pivot, double angle);

		void translate(const Vector3 &translation);

	protected:
		void calculateNormals();
		bool clockwise;
		bool concave;
		std::vector<Vector3> points;
		std::vector<Vector3> normals;
	};
}

#endif