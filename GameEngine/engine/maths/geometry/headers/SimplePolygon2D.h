/*
Author: Jeremy Gillespie-Cloutier
Represents a simple polygon (a polygon that is not self-intersecting).
*/
#ifndef H_SIMPLE_POLYGON
#define H_SIMPLE_POLYGON
#include "Polygon2D.h"

namespace engine {
	class SimplePolygon2D : public Polygon2D {
	public:
		SimplePolygon2D(std::initializer_list<Vector3>, bool);
		SimplePolygon2D(std::vector<Vector3>, bool);
	};
}

#endif