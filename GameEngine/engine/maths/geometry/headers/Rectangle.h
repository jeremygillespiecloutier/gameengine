/*
Author: Jeremy Gillespe-Cloutier
Class that defines a 2D rectangle
*/
#ifndef H_RECTANGLE
#define H_RECTANGLE

#include "SimplePolygon2D.h"

namespace engine {
	class Rectangle : public SimplePolygon2D {
	public:
		Rectangle(Vector3 position, double width, double height);
		Rectangle(double x, double y, double width, double height);
	};
}
#endif
