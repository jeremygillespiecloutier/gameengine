/*
Author: Jeremy Gillespie-Cloutier
Implementation of the Polygon class
*/
#include "Polygon2D.h"
#include <exception>
#include "Utilities.h"
using std::initializer_list;
using std::vector;

namespace engine {

	// Creates a polygon containing the given points
	Polygon2D::Polygon2D(initializer_list<Vector3> points) : Polygon2D(vector<Vector3>(points)) { }

	Polygon2D::Polygon2D(std::vector<Vector3> points) : points(points), normals({}) {
		if (points.size() < 3)
			throw std::exception("A polygon needs at least 3 points");

		// Determine if the polygon is in clockwise order or not, and if it is concave
		double sum = 0;
		concave = false;
		int sign = 1;
		for (unsigned i = 0; i < points.size(); i++) {
			Vector3 &p1 = points[i], &p2 = points[(i + 1) % points.size()], &p3 = points[(i + 2) % points.size()];

			// Check concave
			int currentSign = u_sign((p2.getX() - p1.getX())*(p3.getY() - p2.getY()) - (p3.getX() - p2.getX())*(p2.getY() - p1.getY()));
			if (i == 0)
				sign = currentSign;
			else if (currentSign != sign)
				concave = true;

			// Check order
			Vector3 axis = p2 - p1;
			sum += (p2.getX() - p1.getX())*(p2.getY() + p1.getY());
		}
		clockwise = sum > 0;

		calculateNormals();
	}

	// Determine the polygon normals
	void Polygon2D::calculateNormals() {
		normals.clear();
		for (unsigned i = 0; i < points.size(); i++) {
			Vector3 &p1 = points[i], &p2 = points[(i == points.size() - 1) ? 0 : i + 1];
			Vector3 axis = p2 - p1;
			Vector3 normal = clockwise ? Vector3(-axis.getY(), axis.getX(), 0) : Vector3(axis.getY(), -axis.getX(), 0);
			normal.normalize();
			normals.push_back(normal);
		}
	}

	// Translate the points in the polygon by the given vector
	void Polygon2D::translate(const Vector3 &translation) {
		for (Vector3 &p : points)
			p += translation;
	}

	// Rotate the polygon about the given pivot by the given angle (in degrees)
	void Polygon2D::rotate(const Vector3 &pivot, double angle) {
		for (Vector3 &point : points) {
			Vector3 initial = point - pivot;
			initial.rotate(0, 0, angle);
			point = initial + pivot;
		}
		calculateNormals();
	}

}