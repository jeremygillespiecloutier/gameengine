/*
Author: Jeremy Gillespie-Cloutier
Implementation of the Rectangle class
*/

#include "Rectangle.h"
#include "Vector3.h"
#include "SimplePolygon2D.h"

namespace engine {

	Rectangle::Rectangle(double x, double y, double width, double height) : Rectangle(Vector3(x, y, 0.0), width, height) { }

	Rectangle::Rectangle(Vector3 position, double width, double height) : SimplePolygon2D({
			Vector3(position.getX() - width / 2,position.getY() - width / 2),
			Vector3(position.getX() + width / 2,position.getY() - width / 2),
			Vector3(position.getX() + width / 2,position.getY() + width / 2),
			Vector3(position.getX() - width / 2,position.getY() + width / 2) }, false) {}

}