/*
Author: Jeremy Gillespie-Cloutier
Implementation of the SimplePolygon2D class
*/
#include "SimplePolygon2D.h"
#include "CollisionDetector.h"
using std::initializer_list;
using std::vector;

namespace engine {

	// Creates a simple polygon using the given points.
	// If executeSimpleCheck is true, a check to verify that the points form a simple polygon (not self-intersecting) will be done.
	// If executeSimpleCheck is false, it is assumed that the polygon is simple (even if it is not really).
	//		This improves performance but might cause errors. Should only be used if it is known beforehand that the polygon is simple.
	SimplePolygon2D::SimplePolygon2D(std::initializer_list<Vector3> points, bool executeSimpleCheck = true) : SimplePolygon2D(vector<Vector3>(points), executeSimpleCheck) {}

	SimplePolygon2D::SimplePolygon2D(std::vector<Vector3> points, bool executeSimpleCheck = true) : Polygon2D(points) {
		if (executeSimpleCheck) {
			for (unsigned i = 0;i < points.size(); i++) {
				Vector3 &p1 = points[i];
				Vector3 &p2 = points[(i + 1) % points.size()];

				for (unsigned j = i+1;j < points.size(); j++) {
					Vector3 &q1 = points[j];
					Vector3 &q2 = points[(j + 1) % points.size()];

					if(CollisionDetector::intersect2D(p1, p2, q1, q2))
						throw std::exception("This polygon is not a simple polygon, it is self-intersecting.");
				}
			}
		}
	}

}