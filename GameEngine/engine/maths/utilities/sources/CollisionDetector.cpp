/*
Author: Jeremy Gillespie-Cloutier
Implementation of the CollisionDetector class
*/
#include "CollisionDetector.h"
#include "LineEquation.h"
#include "Vector3.h"
#include <vector>
#include "Utilities.h"
using std::vector;

namespace engine {

	// Determines if the two polygons intersect using the seperating axis theorem test
	bool CollisionDetector::intersect(const Polygon2D &first, const Polygon2D &second) {
		vector<Vector3> points1 = first.getPoints(), points2 = second.getPoints(), normals1 = first.getNormals(), normals2 = second.getNormals();
		vector<Vector3> normals{};
		for (Vector3 &normal : normals1)
			normals.push_back(normal);
		for (Vector3 &normal : normals2)
			normals.push_back(normal);

		for (Vector3 &normal : normals) {
			double min1 = INFINITY, min2 = min1;
			double max1 = -INFINITY, max2 = max1;

			for (Vector3 &p1 : points1) {
				Vector3 proj = Vector3::projection(p1, normal);
				double dist = normal.dot(p1);
				min1 = u_min(min1, dist);
				max1 = u_max(max1, dist);
			}
			for (Vector3 &p2 : points2) {
				double dist = normal.dot(p2);
				min2 = u_min(min2, dist);
				max2 = u_max(max2, dist);
			}
			if (max1 < min2 || max2 < min1) {
				return false;
			}

		}
		return true;
	}

	bool CollisionDetector::intersect(const Polygon2D &poly, const Vector3 &vec) {
		auto points = poly.getPoints();
		unsigned i, j;
		bool collision;
		for (i = 0, j = points.size() - 1, collision = false; i < points.size(); j = i++) {
			auto p1 = points[i], p2 = points[j];
			if ((p1.getY() > vec.getY()) != (p2.getY() > vec.getY()) && (vec.getX() < (p2.getX() - p1.getX())*(vec.getY() - p1.getY()) / (p2.getY() - p1.getY()) + p1.getX()))
				collision = !collision;
		}
		return collision;
	}

	// Returns the points of the second polygon that are located inside the first polygon
	vector<Vector3> CollisionDetector::getContactPoints(const Polygon2D &first, const Polygon2D &second) {
		vector<Vector3> contacts{};
		vector<Vector3> points1 = first.getPoints(), points2 = second.getPoints(), normals1 = first.getNormals();

		for (Vector3 &p : points2) {
			bool collide = true;
			for (unsigned i = 0; i < points1.size(); i++) {
				Vector3 p1 = points1[i], p2 = points1[(i == points1.size() - 1) ? 0 : i + 1];
				Vector3 normal = normals1[i];
				Vector3 diff = p - p1;
				if (diff.dot(normal) > 0) {
					collide = false;
					break;
				}
			}
			if (collide)
				contacts.push_back(p);
		}
		return contacts;
	}

	// Determines if the segments given by (p1 p2) and (q1, q2) intersect themselves.
	bool CollisionDetector::intersect2D(const Vector3 &p1, const Vector3 &p2, const Vector3 &q1, const Vector3 &q2) {
		bool didIntersect = false;
		getIntersection2D(p1, p2, q1, q2, didIntersect);
		return didIntersect;
	}

	// Returns the intersection point between the lines that go through (p1, p2) and (q1, q2).
	// didIntersect (flag): If the line intersection occurs between the segments (p1, p2) and (q1, q2), didIntersect is set to true. Otherwise, it is set to false.
	Vector3 CollisionDetector::getIntersection2D(const Vector3 &p1, const Vector3 &p2, const Vector3 &q1, const Vector3 &q2, bool &didIntersect) {
		didIntersect = false;

		// Find the line equation for the two segments in the form y=a*x+b
		LineEquation eq1{ p1.getX(), p1.getY(), p2.getX(), p2.getY() };
		LineEquation eq2{ q1.getX(), q1.getY(), q2.getX(), q2.getY() };

		// Find the intersection of the two lines
		bool linesIntersected = false; // Flag indicating if the two lines intersected (i.e. non-parallel)
		Vector3 intersection = eq1.intersection(eq2, linesIntersected);

		// Check if the intersection point lies on the segment (p1, p2)
		double norm = (p2 - p1).getNorm();
		if (linesIntersected && (intersection - p1).getNorm() < norm && (intersection - p2).getNorm() < norm)
			didIntersect = true;

		return intersection;
	}

}