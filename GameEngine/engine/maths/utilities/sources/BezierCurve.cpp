/*
Author: Jeremy Gillespie-Cloutier
Implementation of the BezierCurve class
*/
#include "BezierCurve.h"
#include "Utilities.h"

namespace engine {

	BezierCurve::BezierCurve(std::initializer_list<Vector3> points) {
		if (points.size() == 0)
			throw std::exception("A bezier curve needs at least one point");

		for (Vector3 vec : points) {
			this->points.push_back(vec);
		}
	}

	// Gives the point along the curve at parameter t (between 0 and 1)
	Vector3 BezierCurve::pointAt(double t) const {
		t = u_clamp(t, 0.0, 1.0);
		Vector3 sum{};
		unsigned n = points.size() - 1;
		for (unsigned i = 0; i <= n; i++) {
			Vector3 p = points[i];
			sum += u_binomialCoeff(n, i)*u_pow(1 - t, n - i)*u_pow(t, i)*p;
		}
		return sum;
	}

}