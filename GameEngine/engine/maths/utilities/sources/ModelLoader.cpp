/*
Author: Jeremy Gillespie-Cloutier
Implementation of the ModelLoader class
*/
#include "ModelLoader.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include "Vector3.h"
#include <exception>
#include <regex>

namespace engine {

	namespace {

		// Splits the given string using the given string separator
		// Returns a vector containing all the parts of the split string
		// source: The string to split
		// separator: The seperator around which the source string will be split
		std::vector<std::string> splitString(std::string source, std::string seperator) {
			std::vector<std::string> parts{};

			size_t startPosition = source.find_first_not_of(seperator); // Start index of the current part being parsed (inclusive)
			size_t stopPosition = source.find_first_of(seperator, startPosition); // Stop index of the current part being parsed (exclusive)

			while (startPosition != std::string::npos) {
				if (stopPosition == std::string::npos) {
					parts.push_back(source.substr(startPosition));
					break;
				}
				else {
					parts.push_back(source.substr(startPosition, stopPosition - startPosition));
				}
				startPosition = source.find_first_not_of(seperator, stopPosition); // Start index of the current part being parsed (inclusive)
				stopPosition = source.find_first_of(seperator, stopPosition + 1); // Stop index of the current part being parsed (exclusive)
			}

			return parts;
		}

		// Reads the input file line by line and outputs a vector containing each line
		std::vector<std::string> readFile(std::string file) {
			std::vector<std::string> lines{};
			std::ifstream stream;
			stream.open(file);

			if (stream.fail()) {
				throw std::exception("Could not open the file");
			}

			std::string currentLine = "";
			while (std::getline(stream, currentLine)) {
				lines.push_back(currentLine);
			}

			return lines;
		}
	}

	// Creates and returns mesh using the given .obj model
	// file: The .obj file containing the model data
	// [throw]: Exception when the .obj file is not in the right format
	GameMesh* ModelLoader::load(std::string file) {
		MeshData data{};

		// Get the file name from the file path
		// First part of regex (.+[\\/])([^\\/]+) checks a file path including slashes
		// For a path like test/path/ok.test, it will return the match groups 1 and 2 (test/path/)(ok.test)
		// Second part of the regex ([^\\/]+) is for files that don't include slashes
		// For a file like test.jpg, it will match group 3 (test.jpg)
		std::regex fileExpression("^(.+[\\/])([^\\/]+)|([^\\/]+)$");
		std::smatch match;
		std::string fileName;
		std::string filePath = "";
		if (std::regex_search(file, match, fileExpression)) {
			if (match[1].matched) { // Path contains slashes
				filePath = match[1];
				fileName = match[2];
			}
			else if (match[3].matched) { // Path does not contain slashes
				fileName = match[3];
			}
		}

		std::vector<std::string> lines = readFile(file);
		// Parse each line individually
		for (auto str : lines) {
			std::stringstream line(str);
			std::string word;
			line >> word; // First word on the current line
			std::vector<std::string> words = splitString(str, " "); // All the words on the current line

			if (word == "v") { // Vertex
				this->processVertex(line, data);
			}
			else if (word == "vn") { // Vertex normal
				this->processNormal(line, data);
			}
			else if (word == "vt") { // Texture coordinates
				this->processTextureCoords(line, data);
			}
			else if (word == "f") { // Face
				this->processFace(line, data);
			}
			else if (word == "mtllib") { // Load material file
				std::regex materialExpression("^mtllib\\s*(.+)$");
				std::smatch match;
				if (std::regex_search(str, match, materialExpression)) {
					std::string matName = match[1];
					parseMaterial(filePath + matName, filePath, data);
				}
			}
			else if (word == "usemtl" && words.size() >= 2) { // Activate material file
				data.currentMaterial = words[1];
			}
		}

		return new GameMesh(data.vertices, data.vertexNormals, data.faces, data.textureVertices, data.materials);
	}

	void ModelLoader::processVertex(std::stringstream &line, MeshData &data) {
		double v1, v2, v3;
		if (line >> v1 && line >> v2 && line >> v3) {
			data.vertices.push_back(Vector3{ v1, v2, v3 });
		}
		else {
			throw std::exception("Invalid mesh vertex data");
		}
	}

	void ModelLoader::processTextureCoords(std::stringstream &line, MeshData &data) {
		double v1, v2;
		if (line >> v1 && line >> v2) {
			data.textureVertices.push_back(Vector3{ v1, v2, 0 });
		}
		else {
			throw std::exception("Invalid texture coordinate data");
		}
	}

	void ModelLoader::processNormal(std::stringstream &line, MeshData &data) {
		double v1, v2, v3;
		if (line >> v1 && line >> v2 && line >> v3) {
			data.vertexNormals.push_back(Vector3{ v1, v2, v3 });
		}
		else {
			throw std::exception("Invalid mesh vertex normal data");
		}
	}

	void ModelLoader::processFace(std::stringstream &line, MeshData &data) {
		std::string f1, f2, f3;
		if (line >> f1 && line >> f2 && line >> f3) {
			MeshFace face;
			if (data.currentMaterial != "") {
				face.materialName = data.currentMaterial;
				face.hasMaterial = true;
			}

			std::vector<std::string> split1 = splitString(f1, "/");
			std::vector<std::string> split2 = splitString(f2, "/");
			std::vector<std::string> split3 = splitString(f3, "/");

			// 3 possible .obj face formats, based on the type and amount of separators ("/" and "//")
			// Format 1: f v1 v2 v3 (vertices only)
			// Format 2: f v1//vn1 v2//vn2 v3//vn3 (vertices and vertex normals)
			// Format 3: f v1/vt1 v2/vt2 v3/vt3 (vertices and texture vertices)
			// Format 4: f v1/vt1/vn1 v2/vt2/vn2 v3 /vt3/vn3 (vertices, texture vertices and vertex normals)
			size_t doubleSeparator = f1.find("//");
			size_t separator1 = f1.find_first_of("/");
			size_t separator2 = f1.find_last_of("/");

			if (separator1 == std::string::npos) { // Format 1
				face.vertices[0] = stoi(split1[0]) - 1;
				face.vertices[1] = stoi(split2[0]) - 1;
				face.vertices[2] = stoi(split3[0]) - 1;

				face.hasNormals = false;
				face.hasTexture = false;
			}
			else if (doubleSeparator != std::string::npos) { // Format 2
				face.vertices[0] = stoi(split1[0]) - 1;
				face.vertices[1] = stoi(split2[0]) - 1;
				face.vertices[2] = stoi(split3[0]) - 1;

				face.vertexNormals[0] = stoi(split1[1]) - 1;
				face.vertexNormals[1] = stoi(split2[1]) - 1;
				face.vertexNormals[2] = stoi(split3[1]) - 1;

				face.hasNormals = true;
				face.hasTexture = false;
			}
			else if (separator1 != std::string::npos && separator1 == separator2) { // Format 3
				face.vertices[0] = stoi(split1[0]) - 1;
				face.vertices[1] = stoi(split2[0]) - 1;
				face.vertices[2] = stoi(split3[0]) - 1;

				face.textureVertices[0] = stoi(split1[1]) - 1;
				face.textureVertices[1] = stoi(split2[1]) - 1;
				face.textureVertices[2] = stoi(split3[1]) - 1;

				face.hasNormals = false;
				face.hasTexture = true;
			}
			else if (separator1 != std::string::npos && separator2 != std::string::npos && separator1 != separator2) { // Format 4
				face.vertices[0] = stoi(split1[0]) - 1;
				face.vertices[1] = stoi(split2[0]) - 1;
				face.vertices[2] = stoi(split3[0]) - 1;

				face.textureVertices[0] = stoi(split1[1]) - 1;
				face.textureVertices[1] = stoi(split2[1]) - 1;
				face.textureVertices[2] = stoi(split3[1]) - 1;

				face.vertexNormals[0] = stoi(split1[2]) - 1;
				face.vertexNormals[1] = stoi(split2[2]) - 1;
				face.vertexNormals[2] = stoi(split3[2]) - 1;

				face.hasNormals = true;
				face.hasTexture = true;
			}

			data.faces.push_back(face);
		}
		else {
			throw std::exception("Invalid mesh face data");
		}
	}

	GameColor ModelLoader::processColor(std::string line) {
		std::regex expression("^(Ka|Kd|Ks|Ke)\\s+([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)$");
		std::smatch match;
		if (std::regex_search(line, match, expression)) {
			try {
				double r = stod(match[2]);
				double g = stod(match[3]);
				double b = stod(match[4]);

				return GameColor(r, g, b);
			}
			catch (std::exception e) {
				throw std::exception("Invalid color data");
			}
		}
		else {
			throw std::exception("Invalid color data");
		}
	}

	void ModelLoader::parseMaterial(std::string file, std::string path, MeshData &data) {
		std::vector<std::string> lines = readFile(file);

		GameMaterial currentMaterial;
		std::string currentName;
		int materialCount = 0;
		int materialAdded = 0;

		// Parse each line individually
		for (auto str : lines) {
			std::vector<std::string> words = splitString(str, " "); // All the words on the current line
			std::string word = words.size() > 0? words[0] : ""; // First word of the current line

			if (word == "newmtl" && words.size() >= 2) {

				// Save previous material
				if (materialAdded == materialCount - 1) {
					materialAdded++;
					data.materials[currentName] = currentMaterial;
				}

				currentName = words[1];
				currentMaterial = GameMaterial{};
				materialCount++;
			}
			else if (word == "Ka") {
				currentMaterial.setAmbientColor(this->processColor(str));
			}
			else if (word == "Kd") {
				currentMaterial.setDiffuseColor(this->processColor(str));
			}
			else if (word == "Ks") {
				currentMaterial.setSpecularColor(this->processColor(str));
			}
			else if (word == "Ke") {
				currentMaterial.setEmissiveColor(this->processColor(str));
			}
			else if (word == "illum") {
				try {
					int illum = stoi(words[1]);
					currentMaterial.setFlatLighting(illum == 1);
				}
				catch (std::exception e) {
					throw new std::exception("Error parsing illumination model");
				}
			}
			else if (word == "Ns") {
				try {
					double shine = stod(words[1]);
					currentMaterial.setShine(shine);
				}
				catch (std::exception e) {
					throw new std::exception("Error parsing shininess");
				}
			}
			else if ((word == "d" || word == "Tr") && words.size() >= 2) {
				try {
					double alpha = stod(words[1]);
					currentMaterial.setAlpha(alpha);
				}
				catch (std::exception e) {
					throw new std::exception("Error parsing transparency");
				}
			}
			/*
			TODO: Need to add support for the following options:
			-blendu on | off
			-blendv on | off
			-cc on | off
			-clamp on | off
			-mm base gain
			-o u v w
			-s u v w
			-t u v w
			-texres value
			*/
			else if (word == "map_Ka") {
				// TODO
			}
			else if (word == "map_Kd" && words.size() >= 2) {
				std::regex expression("^map_Kd\\s+([^\\s].+)$");
				std::smatch match;
				if (std::regex_search(str, match, expression)) {
					std::string mapName = match[1];
					currentMaterial.setTexture(path+mapName);
				}
			}
		}

		// Ensure last material gets saved
		if (materialAdded == materialCount - 1) {
			materialAdded++;
			data.materials[currentName] = currentMaterial;
		}
	}

}