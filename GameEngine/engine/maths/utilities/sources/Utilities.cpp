/*
Author: Jeremy Gillespie-Cloutier
Implementation of Utilities
*/

#include "Utilities.h"
#include <cmath>
#include <exception>

namespace engine {

	// Returns a^b
	double u_pow(double a, double b) {
		return pow(a, b);
	}

	// Returns n!
	unsigned u_factorial(unsigned n) {
		unsigned ret = 1;
		for (unsigned i = 1; i <= n; i++)
			ret *= i;
		return ret;
	}

	// Returns n choose k
	unsigned u_binomialCoeff(unsigned n, unsigned k) {
		return u_factorial(n) / (u_factorial(k)*u_factorial(n - k));
	}

	// Returns the sign of n
	int u_sign(double n) {
		return n < 0 ? -1 : 1;
	}

}