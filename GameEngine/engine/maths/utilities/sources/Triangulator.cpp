/*
Author: Jeremy Gillespie-Cloutier
Implementation of Triangulator class
*/
#include "Triangulator.h"
#include <iostream>
using std::vector;

namespace engine {

	double getSign(Vector3 a, Vector3 b, Vector3 c) {
		return (a.getX() - c.getX()) * (b.getY() - c.getY()) - (b.getX() - c.getX()) * (a.getY() - c.getY());
	}

	// Determines whether the given point is incide the triangle abc
	bool inTriangle(Vector3 point, Vector3 a, Vector3 b, Vector3 c) {
		bool check1 = getSign(point, a, b) < 0.0;
		bool check2 = getSign(point, b, c) < 0.0;
		bool check3 = getSign(point, c, a) < 0.0;
		return ((check1 == check2) && (check2 == check3));
	}

	using std::vector;

	// Uses the ear clipping algorithm to triangulate the given polygon
	std::vector<Polygon2D> Triangulator::triangulate(const Polygon2D &polygon) {

		vector<Polygon2D> polys{}; // The list of polygons to return
		vector<Vector3> points = polygon.getPoints();

		// Keep clipping ears as long as there are some remaining
		while (points.size() > 3) {

			// Find an ear
			for (unsigned i = 0; i < points.size();i++) {
				// p1, p2 and p3 for the current ear
				int index1 = i, index2 = (i + 1) % points.size(), index3 = (i + 2) % points.size();
				Vector3 p1 = points[index1], p2 = points[index2], p3 = points[index3];

				// check if ear is convex
				double angle = Vector3::angle(p2 - p1, p3 - p2);
				if ((polygon.isClockwise() && angle < 0) || (!polygon.isClockwise() && angle > 0)) {
					// Check all other vertices not part of the current ear
					bool valid = true;
					for (unsigned j = 0; j < points.size(); j++) {
						if (j != index1 && j != index2 && j != index3) {
							// If a vertex is inside the current ear, the diagonal from p1 to p3 intersects another segment, so we cannot remove it
							if (inTriangle(points[j], p1, p2, p3)) {
								valid = false;
								break;
							}
						}
					}
					// If no segment intersects the diagonal p1 p3, then we can convert p1 p2 p3 into a new polygon
					if (valid) {
						Polygon2D p{ p1, p2, p3 };
						polys.push_back(p);
						points.erase(points.begin() + index2); // Remove p2 from the original points
						break;
					}
				}
			}
		}
		polys.push_back(Polygon2D{ points });
		return polys;
	}

}