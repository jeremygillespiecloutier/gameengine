/*
Author: Jeremy Gillespie-Cloutier
Class that can detect collisions between polygons
*/
#ifndef H_COLLISIONDETECTOR
#define H_COLLISIONDETECTOR
#include "Polygon2D.h"
#include "Vector3.h"

namespace engine {

	class CollisionDetector {
	public:
		static bool intersect(const Polygon2D &first, const Polygon2D &second);
		static bool intersect(const Polygon2D &poly, const Vector3 &vec);
		static std::vector<Vector3> getContactPoints(const Polygon2D &first, const Polygon2D &second);
		static bool intersect2D(const Vector3 &p1, const Vector3 &p2, const Vector3 &q1, const Vector3 &q2);
		static Vector3 getIntersection2D(const Vector3 &p1, const Vector3 &p2, const Vector3 &q1, const Vector3 &q2, bool&);
	};

}

#endif