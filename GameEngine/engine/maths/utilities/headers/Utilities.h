/*
Author: Jeremy Gillespie-Cloutier
Various math utility functions
*/
#ifndef H_UTILITIES
#define H_UTILITIES

namespace engine {

	// Clamps the value between minValue and maxValue
	template <typename T> T u_clamp(T value, T minValue, T maxValue) {
		return value < minValue ? minValue : (value > maxValue ? maxValue : value);
	}

	// Returns the min of the two values
	template <typename T> T u_min(T first, T second) {
		return first > second ? second : first;
	}

	// Returns the max of the two values
	template <typename T> T u_max(T first, T second) {
		return first > second ? first : second;
	}


	double u_pow(double a, double b);

	int u_sign(double n);

	unsigned u_factorial(unsigned n);

	unsigned u_binomialCoeff(unsigned n, unsigned k);

}

#endif