/*
Author: Jeremy Gillespie-Cloutier
Class that allows the creation of multi dimensional bezier curves
*/

#ifndef H_BEZIERCURVE
#define H_BEZIERCURVE

#include <initializer_list>
#include <vector>
#include "Vector3.h"

namespace engine {

	class BezierCurve {
	public:
		BezierCurve(std::initializer_list<Vector3> points);
		Vector3 pointAt(double t) const;
	private:
		std::vector<Vector3> points{};
	};

}

#endif