/*
Author: Jeremy Gillespie-Cloutier
Class that triangulates polygons
*/
#ifndef H_TRIANGULATIOR
#define H_TRIANGULATIOR

#include <vector>
#include "Vector3.h"
#include "Polygon2D.h"

namespace engine {

	class Triangulator final {
	public:
		static std::vector<Polygon2D> triangulate(const Polygon2D&);
	};

}

#endif