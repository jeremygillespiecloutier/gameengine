/*
Author: Jeremy Gillespie-Cloutier
Class that loads and parses a .obj file into a mesh
*/
#ifndef H_MODELLOADER
#define H_MODELLOADER
#include <string>
#include <map>
#include "GameMesh.h"
#include "GameColor.h"
#include "GameMaterial.h"

namespace engine {

	namespace {
		struct MeshData {
			std::vector<Vector3> vertices{};
			std::vector<Vector3> vertexNormals{};
			std::vector<Vector3> textureVertices{};
			std::vector<MeshFace> faces{};
			std::map<std::string, GameMaterial> materials{};
			std::string currentMaterial = "";
		};
	}

	class ModelLoader {
	public:
		GameMesh* load(std::string file);
	protected:
		void parseMaterial(std::string file, std::string path, MeshData &data);
		void processVertex(std::stringstream &line, MeshData &data);
		void processNormal(std::stringstream &line, MeshData &data);
		void processFace(std::stringstream &line, MeshData &data);
		void processTextureCoords(std::stringstream &line, MeshData &data);
		GameColor processColor(std::string line);
	};

}

#endif