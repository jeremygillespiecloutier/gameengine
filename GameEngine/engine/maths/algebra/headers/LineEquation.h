/*
Author: Jeremy Gillespie-Cloutier
Class that represents a line equation of the form y=ax+b
*/
#ifndef H_LINE_EQUATION
#define H_LINE_EQUATION

#include <cmath>
#include "Vector3.h"

namespace engine {

	class LineEquation {
	public:

		LineEquation(double x1, double y1, double x2, double y2);

		Vector3 intersection(const LineEquation &other, bool &didIntersect);

	private:
		const double EPSILON = 0.000001;
		double x1, y1, x2, y2; // (x1, y1) and (x2, y2) are 2 points that pass through the line equation
		double slope; // Slope of the line
		double yIntercept; // y-intercept value of the line equation
		bool isInfinite; // Set to true when the slope is infinite (i.e. vertical)
	};

}

#endif