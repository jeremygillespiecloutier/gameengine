/*
Author: Jeremy Gillespie-Cloutier
Implementation of the LineEquation class
*/
#include "LineEquation.h"

namespace engine {

	// Creates a line equation for the line that passes through points (x1, y1) and (x2, y2)
	LineEquation::LineEquation(double x1, double y1, double x2, double y2) : x1(x1), y1(y1), x2(x2), y2(y2) {
		if (abs(x2 - x1) <= EPSILON) {
			isInfinite = true;
		}
		else {
			isInfinite = false;
			slope = (y2 - y1) / (x2 - x1);
			yIntercept = y1 - slope * x1;
		}
	}

	// Returns the intersection between this line with the given line.
	// didIntersect is set to true iff the two lines intersect (i.e. not parallel)
	// Note: does not work for checking collinear lines
	Vector3 LineEquation::intersection(const LineEquation &other, bool &didIntersect) {
		didIntersect = true;

		// Both slopes non vertical and non-identical
		if (!isInfinite && !other.isInfinite && abs(slope - other.slope) > EPSILON) {
			double x = (other.yIntercept - yIntercept) / (slope - other.slope);
			double y = slope * x + yIntercept;
			return Vector3(x, y, 0);
		}
		// Only the slope of this line is vertical
		else if (isInfinite && !other.isInfinite) {
			return Vector3(x1, other.slope*x1 + other.yIntercept, 0);
		}
		// Only the slope of the other line is vertical
		else if (!isInfinite && other.isInfinite) {
			return Vector3(other.x1, slope*other.x1 + yIntercept, 0);
		}
		// Both slopes are identical
		else {
			didIntersect = false;
			return Vector3{};
		}
	}
}