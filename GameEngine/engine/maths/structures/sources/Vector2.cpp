/*
Author: Jeremy Gillespie-Cloutier
Implementation of the Vector2 class
*/

#include "Vector2.h"
#include <cmath>
#include <iostream>
#define PI 3.14159265359
#define DEGREE(X) X*180/PI
#define RAD(X) X*PI/180

namespace engine {

	// Constructor that sets the components to the given values
	Vector2::Vector2(double x, double y) : x(x), y(y) {
		updateNorm();
	}

	// Constructor that makes a copy of the given vector
	Vector2::Vector2(const Vector2& vec) : x(vec.x), y(vec.y), norm(vec.norm) {}

	// Returns a new vector which is the result of the addition of two vectors
	Vector2 operator+(const Vector2& first, const Vector2& second) {
		return Vector2(first.x + second.x, first.y + second.y);
	}

	// Returns a vector which is the result of the substraction of two vectors
	Vector2 operator-(const Vector2& first, const Vector2& second) {
		return Vector2(first.x - second.x, first.y - second.y);
	}

	// Returns a vector that has its components multiplied by the given value
	Vector2 operator*(double val, const Vector2& vec) {
		return Vector2(vec.x*val, vec.y*val);
	}

	// Returns a vector that has its components multiplied by the given value
	Vector2 operator*(const Vector2& vec, double val) {
		return operator*(val, vec);
	}

	// Returns a vector that has its components divided by the given value (only vec/value is defined, since val/vec has no meaning)
	Vector2 operator/(const Vector2& vec, double val) {
		return Vector2(vec.x / val, vec.y / val);
	}

	// Displays this vector in an output stream
	std::ostream& operator<<(std::ostream& out, const Vector2& vec) {
		return out << "(" << vec.x << ", " << vec.y << ")";
	}

	Vector2& Vector2::operator+=(const Vector2& other) {
		x += other.x;
		y += other.y;
		updateNorm();
		return *this;
	}

	Vector2& Vector2::operator-=(const Vector2& other) {
		x -= other.x;
		y -= other.y;
		updateNorm();
		return *this;
	}

	Vector2& Vector2::operator*=(const Vector2& other) {
		x *= other.x;
		y *= other.y;
		updateNorm();
		return *this;
	}

	Vector2& Vector2::set(double x, double y) {
		this->x = x;
		this->y = y;
		updateNorm();
		return *this;
	}

	// normalize the components of the vector
	Vector2& Vector2::normalize() {
		x /= norm;
		y /= norm;
		norm = 1;
		return *this;
	}

	// returns a normalized copy of this vector
	Vector2 Vector2::normalized() const {
		Vector2 copy(*this);
		return copy.normalize();
	}

	// Updates the norm of the vector for the current value of it's components
	double Vector2::updateNorm() {
		return norm = sqrt(x*x + y * y);
	}

	// Returns the dot product of the current vector and the given vector
	double Vector2::dot(const Vector2& other) const {
		return dot(*this, other);
	}

	// Returns a vector which is the cross product of this vector and the given vector
	double Vector2::cross(const Vector2& other) const {
		return cross(*this, other);
	}

	// Returns the cross product of two vectors
	double Vector2::cross(const Vector2& first, const Vector2& second) {
		return first.x*second.y - first.y*second.x;
	}

	// Returns the dot product of two vectors
	double Vector2::dot(const Vector2& first, const Vector2& second) {
		return first.x*second.x + first.y*second.y;
	}

	// Rotate the current vector by the given euler angles (in degrees)
	Vector2& Vector2::rotate(double angle) {
		angle = RAD(angle);
		return set(x*cos(angle) - y * sin(angle), x*sin(angle) + y * cos(angle));
	}

	// Returns the projection of the first vector on the second vector
	Vector2 Vector2::projection(const Vector2& first, const Vector2& second) {
		return (first.dot(second)) / (second.getNorm()*second.getNorm())*second;
	}

	// Returns the signed angle in degrees between the two vectors
	double Vector2::angle(const Vector2& first, const Vector2& second) {
		return DEGREE(atan2(first.x*second.y - first.y*second.x, first.x*second.x + first.y*second.y));
	}

	// Returns the signed angle of the vector in degrees
	double Vector2::angle() const {
		return DEGREE(atan2(y, x));
	}

}