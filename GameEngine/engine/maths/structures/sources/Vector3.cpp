/*
Author: Jeremy Gillespie-Cloutier
Implementation of the Vector3 class
*/

#include "Vector3.h"
#include <cmath>
#include <iostream>
#define PI 3.14159265359
#define DEGREE(X) X*180/PI
#define RAD(X) X*PI/180

namespace engine {

	// Constructor that sets the components to the given values
	Vector3::Vector3(double x, double y, double z) : x(x), y(y), z(z) {
		updateNorm();
	}

	// Constructor that makes a copy of the given vector
	Vector3::Vector3(const Vector3& vec) : x(vec.x), y(vec.y), z(vec.z), norm(vec.norm) {}

	// Returns a new vector which is the result of the addition of two vectors
	Vector3 operator+(const Vector3& first, const Vector3& second) {
		return Vector3(first.x + second.x, first.y + second.y, first.z + second.z);
	}

	// Returns a vector which is the result of the substraction of two vectors
	Vector3 operator-(const Vector3& first, const Vector3& second) {
		return Vector3(first.x - second.x, first.y - second.y, first.z - second.z);
	}

	// Returns a vector that has its components multiplied by the given value
	Vector3 operator*(double val, const Vector3& vec) {
		return Vector3(vec.x*val, vec.y*val, vec.z*val);
	}

	// Returns a vector that has its components multiplied by the given value
	Vector3 operator*(const Vector3& vec, double val) {
		return operator*(val, vec);
	}

	// Returns a vector that has its components divided by the given value (only vec/value is defined, since val/vec has no meaning)
	Vector3 operator/(const Vector3& vec, double val) {
		return Vector3(vec.x / val, vec.y / val, vec.z / val);
	}

	// Displays this vector in an output stream
	std::ostream& operator<<(std::ostream& out, const Vector3& vec) {
		return out << "(" << vec.x << ", " << vec.y << ", " << vec.z << ")";
	}

	Vector3& Vector3::operator+=(const Vector3& other) {
		x += other.x;
		y += other.y;
		z += other.z;
		updateNorm();
		return *this;
	}

	Vector3& Vector3::operator-=(const Vector3& other) {
		x -= other.x;
		y -= other.y;
		z -= other.z;
		updateNorm();
		return *this;
	}

	Vector3& Vector3::operator*=(const Vector3& other) {
		x *= other.x;
		y *= other.y;
		z *= other.z;
		updateNorm();
		return *this;
	}

	Vector3& Vector3::set(double x, double y, double z) {
		this->x = x;
		this->y = y;
		this->z = z;
		updateNorm();
		return *this;
	}

	// normalize the components of the vector
	Vector3& Vector3::normalize() {
		x /= norm;
		y /= norm;
		z /= norm;
		norm = 1;
		return *this;
	}

	// returns a normalized copy of this vector
	Vector3 Vector3::normalized() const {
		Vector3 copy(*this);
		return copy.normalize();
	}

	// Updates the norm of the vector for the current value of it's components
	double Vector3::updateNorm() {
		return norm = sqrt(x*x + y * y + z * z);
	}

	// Returns the dot product of the current vector and the given vector
	double Vector3::dot(const Vector3& other) const {
		return dot(*this, other);
	}

	// Returns a vector which is the cross product of this vector and the given vector
	Vector3 Vector3::cross(const Vector3& other) const {
		return cross(*this, other);
	}

	// Returns the cross product of two vectors
	Vector3 Vector3::cross(const Vector3& first, const Vector3& second) {
		return (first.y*second.z - first.z*second.y)*Vector3(1.0, 0.0, 0.0) + (first.z*second.x - first.x*second.z)*Vector3(0.0, 1.0, 0.0) + (first.x*second.y - first.y*second.x)*Vector3(0.0, 0.0, 1.0);
	}

	// Returns the dot product of two vectors
	double Vector3::dot(const Vector3& first, const Vector3& second) {
		return first.x*second.x + first.y*second.y + first.z*second.z;
	}

	// Rotate the current vector by the given euler angles (in degrees)
	Vector3& Vector3::rotate(double angleX, double angleY, double angleZ) {
		rotateX(angleX);
		rotateY(angleY);
		rotateZ(angleZ);
		return *this;
	}

	void Vector3::rotateX(double angle) {
		angle = RAD(angle);
		double newY = cos(angle)*y - sin(angle)*z;
		double newZ = sin(angle)*y + cos(angle)*z;
		y = newY, z = newZ;
	}

	void Vector3::rotateY(double angle) {
		angle = RAD(angle);
		double newX = cos(angle)*x + sin(angle)*z;
		double newZ = -sin(angle)*x + cos(angle)*z;
		x = newX, z = newZ;
	}

	void Vector3::rotateZ(double angle) {
		angle = RAD(angle);
		double newX = cos(angle)*x - sin(angle)*y;
		double newY = sin(angle)*x + cos(angle)*y;
		x = newX;
		y = newY;
	}

	// Returns the projection of the first vector on the second vector
	Vector3 Vector3::projection(const Vector3& first, const Vector3& second) {
		return (first.dot(second)) / (second.getNorm()*second.getNorm())*second;
	}

	// Returns the signed angle in degrees between the two vectors
	double Vector3::angle(const Vector3& first, const Vector3& second) {
		return DEGREE(atan2(first.x*second.y - first.y*second.x, first.x*second.x + first.y*second.y));
	}

	// Returns the signed angle of the vector relative to the x axis
	double Vector3::angleX() const {
		return angle(Vector3(1, 0, 0), *this);
	}

	// Returns the signed angle of the vector relative to the y axis
	double Vector3::angleY() const {
		return angle(Vector3(0, 1, 0), *this);
	}

	// Returns the signed angle of the vector relative to the z axis
	double Vector3::angleZ() const {
		return angle(Vector3(0, 0, 1), *this);
	}

}