/*
Author: Jeremy Gillespie-Cloutier
Implementation of the Matrix class
*/

#include "Matrix.h"
#include <iostream>

namespace engine {

	// Creates a matrix with given number of rows and cols filled with 0s
	Matrix::Matrix(unsigned rows, unsigned cols) : rows(rows), cols(cols) {
		if (rows == 0 || cols == 0)
			throw std::exception("Matrices must have at least 1 row and 1 column.");

		elements = new double*[rows];
		for (unsigned i = 0; i < rows; i++) {
			elements[i] = new double[cols];
			for (unsigned j = 0; j < cols; j++)
				elements[i][j] = 0;
		}
	}

	void Matrix::clearData() {
		for (unsigned i = 0; i < rows; i++)
			delete[] elements[i];
		delete[] elements;
	}

	Matrix::~Matrix() {
		clearData();
	}

	// Creates a copy of the matrix
	Matrix::Matrix(const Matrix &other): Matrix(other.rows, other.cols) {
		for (unsigned i = 0; i < other.rows; i++)
			for (unsigned j = 0; j < other.cols; j++) 
				elements[i][j] = other.elements[i][j];
	}

	// Adds given matrix to current matrix
	Matrix& Matrix::operator+=(const Matrix &other) {
		for (unsigned i = 0; i < other.rows; i++)
			for (unsigned j = 0; j < other.cols; j++)
				elements[i][j] += other.elements[i][j];
		return *this;
	}

	// Addition of 2 matrices
	Matrix operator+(const Matrix &first, const Matrix &second) {
		if (first.rows != second.rows || first.cols != second.cols)
			throw std::exception("Incompatible matrices");

		Matrix result=Matrix(first);
		result += second;
		return result;
	}

	Matrix& Matrix::operator*=(const Matrix &other) {
		if (cols != other.rows)
			throw std::exception("Incompatible matrix.");

		Matrix result = *this*other;
		clearData();
		rows = result.rows;
		cols = result.cols;
		elements = new double*[rows];
		for (unsigned i = 0; i < rows; i++) {
			elements[i] = new double[cols];
			for (unsigned j = 0; j < cols; j++) 
				elements[i][j] = result.elements[i][j];
		}
		return *this;
	}

	// Multiplication of 2 matrices
	Matrix operator*(const Matrix &first, const Matrix &second) {
		if (first.cols != second.rows)
			throw std::exception("Incompatible matrices");

		Matrix result(first.rows, second.cols);
		for (unsigned i = 0; i < result.rows; i++) {
			for (unsigned j = 0; j < result.cols; j++) {
				double element = 0;
				for (unsigned k = 0; k < first.cols; k++) 
					element += first.elements[i][k] * second.elements[k][j];
				result.elements[i][j] = element;
			}
		}
		return result;
	}

	// Output contents of matrix on stream
	std::ostream& operator<<(std::ostream &stream, const Matrix &matrix) {
		for (unsigned i = 0; i < matrix.rows; i++) {
			for (unsigned j = 0; j < matrix.cols; j++) {
				stream << matrix.elements[i][j];
				if (j < matrix.cols - 1)
					stream << ",";
				else if (i < matrix.rows - 1)
					stream << std::endl;
			}
		}
		return stream;
	}

}