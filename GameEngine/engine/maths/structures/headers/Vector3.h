/*
Author: Jeremy Gillespie-Cloutier
Simple 3 dimensional double vector class containing basic vector operations
*/

#ifndef H_VECTOR3
#define H_VECTOR3

#include <iostream>

namespace engine {

	class Vector3 {

		// Vector operations
		friend Vector3 operator+(const Vector3&, const Vector3&);
		friend Vector3 operator-(const Vector3&, const Vector3&);
		friend Vector3 operator*(double, const Vector3&);
		friend Vector3 operator*(const Vector3&, double);
		friend Vector3 operator/(const Vector3&, double);
		friend std::ostream& operator<<(std::ostream&, const Vector3&);

	public:
		Vector3(double x = 0, double y = 0, double z = 0);
		Vector3(const Vector3&);

		Vector3& operator+=(const Vector3&);
		Vector3& operator-=(const Vector3&);
		Vector3& operator*=(const Vector3&);

		Vector3& normalize();
		Vector3& set(double x, double y, double z);
		Vector3 normalized() const;

		inline double getX() const { return x; }
		inline double getY() const { return y; }
		inline double getZ() const { return z; }
		inline double getNorm() const { return norm; }

		double dot(const Vector3&) const;
		Vector3 cross(const Vector3&) const;
		Vector3& rotate(double angleX, double angleY, double angleZ);
		double angleX() const;
		double angleY() const;
		double angleZ() const;

		static Vector3 cross(const Vector3&, const Vector3&);
		static double dot(const Vector3&, const Vector3&);
		static Vector3 projection(const Vector3&, const Vector3&);
		static double angle(const Vector3&, const Vector3&);

	private:
		double x, y, z, norm;
		double updateNorm();
		void rotateX(double);
		void rotateY(double);
		void rotateZ(double);
	};

}

#endif