/*
Author: Jeremy Gillespie-Cloutier
Matrix class
*/
#ifndef H_MATRIX
#define H_MATRIX

#include <string>
#include <exception>

namespace engine {

	class Matrix {
		friend std::ostream& operator<<(std::ostream &stream, const Matrix &matrix);
		friend Matrix operator+(const Matrix &first, const Matrix &second);
		friend Matrix operator*(const Matrix &first, const Matrix &second);
	public:
		Matrix(unsigned rows, unsigned cols);
		Matrix(const Matrix &other);
		Matrix& operator+=(const Matrix &other);
		Matrix& operator*=(const Matrix &other);
		virtual ~Matrix();

		double get(unsigned i, unsigned j) const {
			return elements[i][j];
		}

		void set(unsigned i, unsigned j, double value) {
			if (i >= rows || j >= cols)
				throw std::exception("Index out of bounds");
			elements[i][j] = value;
		}

		unsigned numRows() const { return this->rows; }
		unsigned numCols() const { return this->cols; }
	private:
		void clearData();
		unsigned rows, cols;
		double **elements;
	};

	template <unsigned NumRows, unsigned NumCols>
	class FixedMatrix : public Matrix {
	public:
		FixedMatrix() : Matrix(NumRows, NumCols) {}
	};

}

#endif