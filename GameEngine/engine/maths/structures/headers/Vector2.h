/*
Author: Jeremy Gillespie-Cloutier
Simple 3 dimensional double vector class containing basic vector operations
*/

#ifndef H_Vector2
#define H_Vector2

#include <iostream>

namespace engine {

	class Vector2 {

		// Vector operations
		friend Vector2 operator+(const Vector2&, const Vector2&);
		friend Vector2 operator-(const Vector2&, const Vector2&);
		friend Vector2 operator*(double, const Vector2&);
		friend Vector2 operator*(const Vector2&, double);
		friend Vector2 operator/(const Vector2&, double);
		friend std::ostream& operator<<(std::ostream&, const Vector2&);

	public:
		Vector2(double x = 0, double y = 0);
		Vector2(const Vector2&);

		Vector2& operator+=(const Vector2&);
		Vector2& operator-=(const Vector2&);
		Vector2& operator*=(const Vector2&);

		Vector2& normalize();
		Vector2& set(double x, double y);
		Vector2 normalized() const;

		inline double getX() const { return x; }
		inline double getY() const { return y; }
		inline double getNorm() const { return norm; }

		double dot(const Vector2&) const;
		double cross(const Vector2&) const;
		Vector2& rotate(double angle);
		double angle() const;

		static double cross(const Vector2&, const Vector2&);
		static double dot(const Vector2&, const Vector2&);
		static Vector2 projection(const Vector2&, const Vector2&);
		static double angle(const Vector2&, const Vector2&);

	private:
		double x, y, norm;
		double updateNorm();
	};

}

#endif