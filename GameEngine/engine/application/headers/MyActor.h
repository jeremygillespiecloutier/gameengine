#ifndef H_MYACTOR
#define H_MYACTOR

#include "GameActor.h"

class MyActor final: public engine::GameActor {
private:
	void display() override;
	void update(unsigned deltaTime) override;
};
#endif
