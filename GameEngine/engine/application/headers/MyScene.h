/*
Author: Jeremy Gillespie-Cloutier
Test scene to try out the engine
*/
#ifndef H_MYSCENE
#define H_MYSCENE

#include "GameScene.h"

class MyScene final: public engine::GameScene {
public:
	MyScene();
	~MyScene();
private:
	void display() override;
	void update(unsigned deltaTime) override;
};

#endif