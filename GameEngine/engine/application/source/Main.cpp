/*
Author: Jeremy Gillespie-Cloutier
Test application to showcase the game engine usage
*/
#include <glew.h>
#include "GameWindow.h"
#include "MyActor.h"
#include "MyScene.h"

using namespace engine;

int main(int argc, char **argv) {
	// Create a window
	GameWindow test(1200, 800, "test");
	// Create a scene
	MyScene scene;
	// Create an actor
	MyActor actor;
	test.setScene(scene);
	test.start();
	return 0;
}