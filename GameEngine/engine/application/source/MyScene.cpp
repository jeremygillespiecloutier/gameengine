/*
Author: Jeremy Gillespie-Cloutier
Implementation of MyScene, for test purposes
*/
#include "MyScene.h"
#include "MyActor.h"
#include "GameRenderer.h"
#include "GameColor.h"
#include "GameSprite.h"
#include "Triangulator.h"
#include "Polygon2D.h"
#include "Rectangle.h"
#include "Vector3.h"
#include "CollisionDetector.h"
#include "PFNode.h"
#include "PFLink.h"
#include "Pathfinder.h"
#include "GameAnimation.h"
#include <string>
#include <functional>
#include "Matrix.h"
#include "BezierCurve.h"
#include "GameInput.h"
#include "SimplePolygon2D.h"
#include "GameWindow.h"
#include "ModelLoader.h"
#include "GameMesh.h"
#include "GameShader.h"

using namespace engine;
using std::cout;
using std::endl;
using std::string;
using std::function;

std::vector<Polygon2D> triangulation;
Polygon2D gon{ Vector3{},Vector3{}, Vector3{} }, gon2{ Vector3{},Vector3{}, Vector3{} };
GameSprite *sprite = nullptr, *sprite2 = nullptr;
engine::Rectangle *first, *second;
std::vector<Vector3> points{};
MyActor actor{}, actor2{};
GameAnimation *animation;
BezierCurve *curve;
PerspectiveCamera *camera1;
OrtographicCamera *camera2;
GameMesh *mesh;
GameMesh *mesh2;
GameLight *light;
GameLight *light2;
GameShader *colorShader1;
GameShader *colorShader2;
GameShader *textureShader;
GameShader *textureShader2;
double rotation = 0;
double rotationSpeed = 180;

MyScene::MyScene() : GameScene() {
	sprite = new GameSprite("data/images/test.jpg", 100, 100);
	actor.addComponent(*sprite);
	actor.getTransform().translateBy(500, 0, 0);
	sprite2 = new GameSprite("data/images/test2.jpg", 100, 100, 50, 50);
	first = new engine::Rectangle(50, 50, 100, 100);
	second = new engine::Rectangle(50, 50, 80, 80);
	bool collide = CollisionDetector::intersect(*first, *second);
	points = CollisionDetector::getContactPoints(*first, *second);

	string a = "A", b = "B", c = "C", d = "D", e = "E";
	PFNode<string> A(a), B(b), C(c), D(d), E(e);
	PFLink<string> l1(B, 1); A.addLink(l1);
	PFLink<string> l2(C, 4); A.addLink(l2);
	PFLink<string> l3(E, 5); A.addLink(l3);
	PFLink<string> l4(D, 1); B.addLink(l4);
	PFLink<string> l5(E, 1); D.addLink(l5);
	vector<PFNode<string>*> path = Pathfinder::findPath(A, E);
	for (auto *test : path)
		std::cout << test->getElement() << std::endl;
	l5.setActive(false);
	D.removeLink(l5);
	path = Pathfinder::findPath(A, E);
	for (auto *test : path)
		std::cout << test->getElement() << std::endl;
	animation = new GameAnimation("data/animations/Run__", "png", 0, 9, 3);
	animation->setPosition(300, 0);

	double e1[4][3] = {
		{ 1,0,0 },
		{ 0,0,0 },
		{ 0,4,0 },
		{ 2,0,0 }
	};
	double e2[3][4] = {
		{ 0,0,0,0 },
		{ 0,2,5,0 },
		{ 1,0,3,0 }
	};
	Matrix m1(4, 3), m2(3, 4);
	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 3; j++)
			m1.set(i, j, e1[i][j]);
	for (unsigned i = 0; i < 3; i++)
		for (unsigned j = 0; j < 4; j++)
			m2.set(i, j, e2[i][j]);
	std::cout << std::endl;
	std::cout << m1 * m2 << std::endl;
	m1 *= m2;
	std::cout << std::endl;
	std::cout << m1 << std::endl;
	curve = new BezierCurve{ Vector3{0, 0, 0}, Vector3{400, 400, 0}, Vector3{800, 0, 0} };
	std::cout << curve->pointAt(0.5) << std::endl;

	try {
		vector<Vector3> points1 = { Vector3(0, 0, 0), Vector3(2, 2, 0), Vector3(1, 4, 0), Vector3(3, 4, 0), Vector3(3, 0, 0) };
		SimplePolygon2D simple1{ points1, true };
		std::cout << "POLYGON 1 IS A SIMPLE POLYGON" << std::endl;
	}
	catch (...) {
		std::cout << "POLYGON 1 IS NOT A SIMPLE POLYGON" << std::endl;
	}

	try {
		vector<Vector3> points2 = { Vector3(0, 0, 0), Vector3(2, 2, 0), Vector3(0, 2, 0), Vector3(2, 0, 0) };
		SimplePolygon2D simple2{ points2 , true };
		std::cout << "POLYGON 2 IS A SIMPLE POLYGON" << std::endl;
	}
	catch (...) {
		std::cout << "POLYGON 2 IS NOT A SIMPLE POLYGON" << std::endl;
	}
	camera1 = new PerspectiveCamera(ViewArea{ 100.0, 0.0, (double)GameWindow::getWidth() , (double)GameWindow::getHeight() }, -1, 1, -1, 1, 1, 100);
	camera2 = new OrtographicCamera(ViewArea{ 0.0, 0.0, (double)GameWindow::getWidth() , (double)GameWindow::getHeight() }, 1200, 800, -5, 5);

	ModelLoader loader{};
	mesh = loader.load("data/models/teapot.obj");
	mesh2 = loader.load("data/models/Fox/Fox Assault.obj");

	actor.addComponent(*animation);
	actor.setCamera(camera2);
	addActor(&actor);
	animation->play();

	actor2.setCamera(camera1);
	actor2.addComponent(*mesh2);
	actor2.getTransform().scaleBy(Vector3{ 2, 2, 2 });
	addActor(&actor2);

	glTranslated(0.6, 0.3, -25.0);
	actor2.getTransform().translateBy(Vector3{ -0.5, 0.3, -25.0 });

	light = new PointLight(Vector3(0, 1, -2), GameColor { 1.0, 1.0, 1.0, 1 });
	this->addLight(light);

	light2 = new DirectionalLight(Vector3(1, 1, 0), GameColor{ 1.0, 1.0, 1.0, 1 });
	this->addLight(light2);

	colorShader1 = new ColorShader{ "data/shaders/colorShader/colorVertex.glsl", "data/shaders/colorShader/colorFragment.glsl"};
	colorShader2 = new ColorShader{ "data/shaders/colorShader/colorVertex.glsl", "data/shaders/colorShader/colorFragment.glsl", {"ENABLE_LIGHT"} };
	textureShader = new TextureShader{ "data/shaders/textureShader/textureVertex.glsl", "data/shaders/textureShader/textureFragment.glsl" };
	textureShader2 = new TextureShader{ "data/shaders/textureShader/textureVertex.glsl", "data/shaders/textureShader/textureFragment.glsl", {"ENABLE_LIGHT"} };
	this->attachShader(textureShader2);

	GameRenderer::setShader(colorShader1);
	actor2.setShader(textureShader2);
	actor.setShader(textureShader);
}

void MyScene::update(unsigned deltaTime) {
	vector<Vector3> points1{
		Vector3(0,0,0),
		Vector3(200,0,0),
		Vector3(180,50,0),
		Vector3(250,50,0),
		Vector3(200, -200, 0),
		Vector3(100,-50,0),
		Vector3(100,-100,0)
	};
	gon = Polygon2D(points1);
	gon.translate(Vector3(300, 300, 0));
	gon.rotate(Vector3(-50, -50, 0), 35);
	vector <Vector3> points2 = {
		Vector3(-50,-50,0),
		Vector3(50,-50,0),
		Vector3(50,50,0),
		Vector3(-50,50,0)
	};
	gon2 = Polygon2D{ points2 };
	gon2.rotate(Vector3{}, 35);
	gon2.translate(GameInput::getMousePosition());
	triangulation = Triangulator::triangulate(gon);
	bool collisionMouse = CollisionDetector::intersect(gon, GameInput::getMousePosition());
	string info = collisionMouse ? "MOUSE COLLISION, " : "NO MOUSE COLLISION, ";
	bool collision = false;
	for (Polygon2D &tri : triangulation) {
		collision = collision || CollisionDetector::intersect(tri, gon2);
		if (collision)
			break;
	}
	info += collision ? "RECTANGLE COLLISION" : "NO RECTANGLE COLLISION";
	std::cout << info << std::endl;
	actor2.getTransform().rotateBy(rotationSpeed*deltaTime / 1000, rotationSpeed*deltaTime / 1000, 0);
}

void MyScene::display() {
	this->setCamera(*camera2);

	GameRenderer::setShader(textureShader);
	//GameRenderer::drawSprite(*sprite2);
	GameRenderer::setShader(colorShader1);
	GameRenderer::setColor(0, 1, 1, 1);
	GameRenderer::drawLine(0, 0, 100, 100);
	GameRenderer::drawRectangle(300, 300, 200, 200);
	GameRenderer::fillRectangle(200, 200, 25, 25);
	GameRenderer::setColor(1, 0, 0, 1);
	GameRenderer::fillRectangle(300, 300, 200, 200);
	GameRenderer::fillRectangle(0, 0, 15, 15);
	GameRenderer::setLineWidth(3);
	GameRenderer::setLineWidth(1);
	GameRenderer::setColor(0, 0, 1, 1);
	for (unsigned i = 0; i < triangulation.size(); i++)
		GameRenderer::drawPolygon(triangulation[i]);
	GameRenderer::drawPolygon(gon2);
	glPushMatrix();
	glTranslated(300, 300, 0);

	GameRenderer::drawEllipse(100, 100, 100, 100, 25);
	GameRenderer::setColor(1, 1, 0, 1);
	GameRenderer::fillRectangle(*first);
	GameRenderer::setColor(0, 1, 0, 1);
	GameRenderer::drawRectangle(*second);
	GameRenderer::setColor(0.5, 0.5, 0.5, 1);
	glPushMatrix();
	glTranslated(0, 0, 1);
	for (Vector3 point : points) {
		GameRenderer::drawEllipse(point.getX(), point.getY(), 5, 5, 10);
	}
	GameRenderer::setColor(1, 0, 0, 0.3);
	GameRenderer::fillEllipse(100, 100, 100, 100, 25);
	GameRenderer::fillTriangle(0, 0, 400, 400, 200, 600);
	glPopMatrix();

	glPopMatrix();
	GameRenderer::drawBezier(*curve, 100);

	glPushMatrix();
	glTranslated(1, 0, 0);
	GameRenderer::beginRender(RenderType::LineLoop);
	GameRenderer::setColor(0, 0, 1, 1);
	GameRenderer::pushVertex(0, 0, 0);
	GameRenderer::pushVertex(1, 0, 0);
	GameRenderer::pushVertex(1, 1, 0);
	GameRenderer::endRender();
	glPopMatrix();
}

MyScene::~MyScene() {
	// This is just a test class
	delete camera1;
	delete camera2;
	delete mesh;
	delete mesh2;
	delete light;
	delete colorShader1;
	delete colorShader2;
	delete textureShader;
}