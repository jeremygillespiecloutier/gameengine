/*
Author: Jeremy Gillespie-Cloutier
Class that implements the A* pathfinding algorithm
*/
#ifndef H_PATHFINDER
#define H_PATHFINDER
#include <vector>
#include <functional>
#include "PFNode.h"
#include "PFLink.h"

// Declaration of the class
namespace engine {

	class Pathfinder final {
	public:
		template <typename T> static std::vector<PFNode<T>*> findPath(PFNode<T> &start, PFNode<T> &stop,
			std::function<int(T&, T&)> heuristic = function<int(T&, T&)>([](T&, T&)->int {return 0; }));
	};

}

// Implementation of the class
namespace engine {

	using std::vector;
	using std::function;

	// Determines if the given node is contained in the given vector of nodes
	template <typename T> bool contains(vector<PFNode<T>*> &nodes, PFNode<T> *node) {
		for (auto *n : nodes)
			if (n == node)
				return true;
		return false;
	}

	/*
	* Returns the shortest path between the two nodes using the provided heuristic function
	* The return value is a vector of nodes [v1, v2, ..., vn] such that the shortest path between v1 and vn is {v1,v2,...,vn-1,vn} (empty if no path found)
	*/
	template <typename T> std::vector<PFNode<T>*> Pathfinder::findPath(PFNode<T> &start, PFNode<T> &stop, function<int(T&, T&)> heuristic) {
		// The vector that will hold the path, and the closed (visited) and open (reachable) nodes
		std::vector<PFNode<T>*> path, open, closed;
		// Add the start node to open list
		open.push_back(&start);
		PFNode<T>* current = nullptr;
		bool found = false;

		while (!open.empty()) { // As long as there are still nodes to visit
			// Find the best node (least f value)
			unsigned index = 0;
			current = open[0];
			for (unsigned i = 1; i < open.size(); i++) {
				if (open[i]->f < current->f) {
					index = i;
					current = open[i];
				}
			}
			// Move current node from open to closed list
			open.erase(open.begin() + index);
			closed.push_back(current);
			// If current node is destination, we are done
			if (current == &stop) {
				found = true;
				break;
			}

			// Otherwise expand current node
			for (PFLink<T> *link : current->links) {
				if (link->active && !contains(closed, link->destination)) { // Node has not been visited and is reachable

					int currH = heuristic(link->destination->element, stop.element);
					int currG = current->g + link->cost;
					int currF = currG + currH;
					// If expanded node we found is better than before, update it
					if (contains(open, link->destination) && currF < link->destination->f) {
						link->destination->h = currH;
						link->destination->g = currG;
						link->destination->f = currF;
						link->destination->pred = current;
					}
					else { // Otherwise add expanded node to open list
						link->destination->h = currH;
						link->destination->g = currG;
						link->destination->f = currF;
						link->destination->pred = current;
						open.push_back(link->destination);
					}
				}
			}
		}
		// Recreate the path from start to stop
		if (found) {
			while (current != nullptr) {
				path.insert(path.begin(), current);
				current = current->pred;
			}
		}
		// Reset the node values that were changed
		for (PFNode<T> *node : open) {
			node->f = 0;
			node->g = 0;
			node->h = 0;
			node->pred = nullptr;

		}
		for (PFNode<T> *node : closed) {
			node->f = 0;
			node->g = 0;
			node->h = 0;
			node->pred = nullptr;
		}
		return path;
	}

}

#endif
