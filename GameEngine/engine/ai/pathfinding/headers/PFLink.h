/*
Author: Jeremy Gillespie-Cloutier
Class that represents a link between 2 nodes in pathfinding
*/
#ifndef H_PFLINK
#define H_PFLINK

// Declaration of the class
namespace engine {

	class Pathfinder;
	template <typename T> class PFNode;

	template <typename T>
	class PFLink {
		friend class Pathfinder;
	public:
		PFLink(PFNode<T> &destination, int cost, bool active = true);
		inline void setActive(bool active) { this->active = active; } // Toggle the active state
	private:
		PFNode<T> *destination = nullptr; // The destination node of this link
		int cost; // The cost of following this link
		bool active; // Flag indicating if the link can be travelled
	};

}

// Implementation of the class
namespace engine {

	template <typename T> PFLink<T>::PFLink(PFNode<T> &destination, int cost, bool active) : destination(&destination), cost(cost), active(active) {}

}

#endif