/*
Author: Jeremy Gillespie-Cloutier
Class that defines a node used for pathfinding
*/
#ifndef H_PFNODE
#define H_PFNODE
#include <vector>

// Declaration of the class
namespace engine {

	class Pathfinder;
	template <typename T> class PFLink;

	template <typename T>
	class PFNode {
		friend class Pathfinder;
	public:
		PFNode(T &elem);
		void addLink(PFLink<T>&);
		void removeLink(PFLink<T>&);
		inline T& getElement() { return element; }
	private:
		T & element; // The element held by the node
		int f = 0, g = 0, h = 0; // The distance (g), heuristic (h) and total cost (f) of the current node
		std::vector<PFLink<T>*> links; // The links this node has to other nodes
		PFNode *pred = nullptr; // The predecessor of this node in pathfinding
	};
}

// Implementation of the class
namespace engine {

	template <typename T> PFNode<T>::PFNode(T &element) : element(element) {}

	// Add a link to the list of links
	template <typename T> void PFNode<T>::addLink(PFLink<T> &link) {
		links.push_back(&link);
	}

	// Remove the given link from the list of links
	template <typename T> void PFNode<T>::removeLink(PFLink<T> &link) {
		for (unsigned i = 0; i < links.size(); i++)
			if (links[i] == &link) {
				links.erase(links.begin() + i);
				return;
			}
	}

}

#endif
